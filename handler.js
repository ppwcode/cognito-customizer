// Trigger: Pre token generation, `TokenGeneration_RefreshTokens`
// https://docs.aws.amazon.com/cognito/latest/developerguide/cognito-user-identity-pools-working-with-aws-lambda-triggers.html
// https://docs.aws.amazon.com/cognito/latest/developerguide/user-pool-lambda-pre-token-generation.html
async function adornToken (event) {
  console.log('adornToken called; event:', event)
  const clientId = event.callerContext.clientId
  console.log('clientId:', clientId)
  const userName = event.userName
  console.log('userName:', userName)
  const userAttributes = event.request.userAttributes
  console.log('userAttributes:', userAttributes)
  event.response = {
    claimsOverrideDetails: {
      claimsToAddOrOverride: {
        resourceServerRole: 'this is a test role',
        resourceServerCustomerId: 999999
      }
    }
  }
  console.log('adornToken done; event:', event)
  return event
}

module.exports = { adornToken }
