/* eslint-env mocha */

const jwtError = require('../lib/jwtError')
const jwtSpec = require('../lib/jwtSpec')
const x = require('cartesian')

const cases = x({
  errorCode: Object.keys(jwtSpec.tokenRequestErrorStatusCodes),
  description: [undefined, 'this is a description'],
  uri: [undefined, 'https://example.org/description']
})

describe('10.jwtError', function () {
  beforeEach(function () {
    jwtError.contract.verifyPostconditions = true
  })

  afterEach(function () {
    jwtError.contract.verifyPostconditions = false
  })

  describe('base function', function () {
    cases.forEach(c => {
      it(`Boom for ${c.errorCode}, with description ${c.description}, and uri ${c.uri}`, function () {
        const result = jwtError(c.errorCode, c.description, c.uri)
        console.log(result)
      })
    })
  })
  describe('specific functions', function () {
    cases.forEach(c => {
      it(`has a function for ${c.errorCode}`, function () {
        jwtError[c.errorCode].should.be.Function()
      })
      it(`Boom for ${c.errorCode}, with description ${c.description}, and uri ${c.uri}`, function () {
        const result = jwtError[c.errorCode](c.description, c.uri)
        console.log(result)
      })
    })
  })
})
