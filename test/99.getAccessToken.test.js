/* eslint-env mocha */

const proxyquire = require('proxyquire')
const payloadDecoratorContract = require('../lib/payloadDecoratorContract')
const cases = require('./_cases')
const sinon = require('sinon')
const { getSigningKey } = require('../lib/userPool/signingKeyGetter')
const stsConfig = require('./config/sts')
const should = require('should')
const getUserPoolTokensContract = require('../lib/userPool/getTokens').contract
const extractJwtPayloadContract = require('../lib/extractJwtPayload').contract
const Joi = require('joi')
const signJwtContract = require('../lib/signJwt').contract
const jwtSpec = require('../lib/jwtSpec')
const flowLogger = require('../lib/_util/flowLogger')

const getUserPoolTokensImpl = sinon.stub()
const getUserPoolTokens = getUserPoolTokensContract.implementation(getUserPoolTokensImpl)

const extractJwtPayloadImpl = sinon.stub()
const extractJwtPayload = extractJwtPayloadContract.implementation(extractJwtPayloadImpl)

const signJwtImpl = sinon.stub()
const signJwt = signJwtContract.implementation(signJwtImpl)
const finalAccessToken = 'DUMMY SIGNED FINAL ACCESS TOKEN'
const signJwtResult = {
  header: {
    alg: jwtSpec.algorithm,
    kid: '708c8e8f-67c5-44f3-b98a-26537dfd0d5a', // MUDO copied from dummy implementation in getAccessToken
    typ: 'JWT'
  },
  token: finalAccessToken
}

const getAccessToken = /** @type {ContractFunction} */ proxyquire('../lib/getAccessToken', {
  './userPool/getTokens': getUserPoolTokens,
  './extractJwtPayload': extractJwtPayload,
  './signJwt': signJwt
})

const decoratorImpl = sinon.stub()
const decorator = payloadDecoratorContract.implementation(decoratorImpl)

const userPoolSigningKeyGetterImpl = sinon.stub()
const userPoolSigningKeyGetter = getSigningKey.implementation(userPoolSigningKeyGetterImpl)

const configuration = {
  payloadDecorator: decorator,
  issuer: cases.issuer,
  defaultExpirationSeconds: cases.expiresInSeconds,
  userPoolSigningKeyGetter,
  userPoolTokenEndpoint: stsConfig.tokenEndpoint
}

function shouldBeExpectedBasePayload (payload) {
  payload.should.be.an.Object()
  should(
    Joi.string()
      .uuid({ version: 'uuidv4' })
      .validate(payload.jti).error
  ).not.ok()
  payload.token_use.should.equal('access')
  payload.iss.should.equal(configuration.issuer)
  payload.client_id.should.equal(cases.userPoolAccessTokenPayload.client_id)
  payload.sub.should.equal(cases.userPoolAccessTokenPayload.sub)
  payload.aud.should.equal(cases.userPoolAccessTokenPayload.aud)
  payload.scope.should.equal(cases.userPoolAccessTokenPayload.scope)
  payload.iat.should.equal(cases.issuedAtSecSinceEpoch)
  payload.nbf.should.equal(cases.issuedAtSecSinceEpoch)
  payload.exp.should.equal(cases.issuedAtSecSinceEpoch + configuration.defaultExpirationSeconds)
}

describe('99.getAccessToken', function () {
  beforeEach(function () {
    getAccessToken.contract.verifyPostconditions = true
    getUserPoolTokensContract.verifyPostconditions = true
    extractJwtPayloadContract.verifyPostconditions = true
    extractJwtPayloadImpl.resolves(cases.userPoolAccessTokenPayload)
    signJwtContract.verifyPostconditions = true
    signJwtImpl.resolves(signJwtResult)
    decoratorImpl.resolves({})
    this.logger = flowLogger(cases.sot, cases.flowId, 'trace')
  })

  afterEach(function () {
    decoratorImpl.reset()
    signJwtImpl.reset()
    signJwtContract.verifyPostconditions = false
    extractJwtPayloadImpl.reset()
    extractJwtPayloadContract.verifyPostconditions = false
    getUserPoolTokensImpl.reset()
    getUserPoolTokensContract.verifyPostconditions = false
    getAccessToken.contract.verifyPostconditions = false
  })

  cases.tokenRequests.forEach(tr => {
    function shouldBeExpectedStructuredRequest (request) {
      request.should.be.an.Object()
      tr.request.should.containDeep(request)
      tr.request.body.should.have.property('extraBodyParameter')
      // double check for security leak
      request.headers.should.be.an.Object()
      request.headers.should.not.have.property('authorization')
      request.body.should.be.an.Object()
      request.body.should.not.have.property('code')
      request.body.should.not.have.property('code_verifier')
      request.body.should.not.have.property('refresh_token')
    }

    it(`returns a bearer access token when all is well with ${tr.description}`, async function () {
      getUserPoolTokensImpl.resolves(tr.userPoolResponse)
      const result = await getAccessToken(configuration, this.logger, cases.sot, tr.request)
      console.log(result)
      console.log()
      sinon.assert.calledOnce(getUserPoolTokensImpl)
      sinon.assert.calledWith(getUserPoolTokensImpl, configuration, sinon.match({}), tr.request)
      sinon.assert.calledOnce(extractJwtPayloadImpl)
      sinon.assert.calledWith(
        extractJwtPayloadImpl,
        sinon.match({}), // a child logger
        configuration.userPoolSigningKeyGetter,
        cases.sot,
        tr.userPoolResponse.access_token
      )
      // MUDO check call for keypair
      sinon.assert.calledOnce(decoratorImpl)
      const decoratorCallArgs = decoratorImpl.getCall(0).args
      shouldBeExpectedBasePayload(decoratorCallArgs[2])
      shouldBeExpectedStructuredRequest(decoratorCallArgs[1])
      sinon.assert.calledOnce(signJwtImpl)
      const signJwtCallArgs = signJwtImpl.getCall(0).args
      shouldBeExpectedBasePayload(signJwtCallArgs[1])
      result.should.be.an.Object()
      result.access_token.should.equal(finalAccessToken)
      result.token_type.should.equal('bearer')
      result.expires_in.should.equal(configuration.defaultExpirationSeconds)
      switch (tr.description) {
        case 'authorization_code':
          result.refresh_token.should.equal(cases.newRefreshToken)
          break
        case 'refresh_token':
          // because cognito does not return a new refresh token
          result.refresh_token.should.equal(cases.oldRefreshToken)
          break
        case 'client_credentials':
          should(result.refresh_token).be.undefined()
          break
        default:
          should().fail()
      }
      result.scope.should.equal(cases.userPoolAccessTokenPayload.scope)
    })
  })
})
