/* eslint-env mocha */

const decoratePayload = require('../lib/decoratePayload')
const payloadDecoratorContract = require('../lib/payloadDecoratorContract')
const cases = require('./_cases')
const sinon = require('sinon')
const Hoek = require('hoek')
const jwtError = require('../lib/jwtError')
const PostconditionViolation = require('@toryt/contracts-iv/lib/IV/PostconditionViolation')
const ExceptionConditionViolation = require('@toryt/contracts-iv/lib/IV/ExceptionConditionViolation')
const flowLogger = require('../lib/_util/flowLogger')

const decoratorImpl = sinon.stub()

const decorator = payloadDecoratorContract.implementation(decoratorImpl)

function shouldBeExpectedRequest (originalRequest, calledWith) {
  calledWith.should.not.equal(originalRequest)
  calledWith.headers.should.be.an.Object()
  calledWith.headers.should.not.have.property('authorization')
  calledWith.headers.should.not.have.property('accept')
  calledWith.headers.should.not.have.property('content-type')
  calledWith.body.should.be.an.Object()
  calledWith.body.should.not.have.property('client_id')
  calledWith.body.should.not.have.property('aud')
  calledWith.body.should.not.have.property('code')
  calledWith.body.should.not.have.property('code_verifier')
  calledWith.body.should.not.have.property('refresh_token')
  calledWith.body.should.not.have.property('redirect_uri')
  const expectedRequest = Hoek.clone(originalRequest)
  delete expectedRequest.headers.authorization
  delete expectedRequest.headers.accept
  delete expectedRequest.headers['content-type']
  delete expectedRequest.body.client_id
  delete expectedRequest.body.aud
  delete expectedRequest.body.code
  delete expectedRequest.body.code_verifier
  delete expectedRequest.body.refresh_token
  delete expectedRequest.body.redirect_uri
  calledWith.should.deepEqual(expectedRequest)
}

describe('70.decoratePayload', function () {
  beforeEach(function () {
    decoratePayload.contract.verifyPostconditions = true
    // NOTE: decorator contract verifyPostconditions is already true
    this.logger = flowLogger(cases.sot, cases.flowId, 'trace')
  })

  afterEach(function () {
    decoratePayload.contract.verifyPostconditions = false
    decoratorImpl.reset()
  })

  describe('nominal', function () {
    function generateTests (decoratorResolution, expectedResult) {
      cases.tokenRequests.forEach(tr => {
        it(`decorates a payload correctly, with an ${tr.description} request`, async function () {
          decoratorImpl.resolves(Hoek.clone(decoratorResolution))
          const result = await decoratePayload(this.logger, decorator, tr.request, cases.baseAccessTokenPayload)
          console.log('result')
          console.log(result)
          console.log()
          result.should.eql(expectedResult)
          sinon.assert.calledOnce(decoratorImpl)
          const call = decoratorImpl.getCall(0)
          shouldBeExpectedRequest(tr.request, call.args[1])
          call.args[2].should.not.equal(cases.baseAccessTokenPayload)
          call.args[2].should.deepEqual(cases.baseAccessTokenPayload)
        })
      })
    }

    describe('no extra claims', function () {
      generateTests({}, cases.baseAccessTokenPayload)
    })
    describe('with extra claims', function () {
      generateTests(cases.extraAccessTokenClaims, cases.accessTokenPayload)
    })
    describe('with claim replacements', function () {
      const exp = cases.issuedAtSecSinceEpoch + 1e6 // bigger
      const nbf = Math.floor((cases.issuedAtSecSinceEpoch + cases.expirySecSinceEpoch) / 2)
      const scope = 'another scope'
      const changes = [
        { description: 'exp', decorations: { exp } },
        { description: 'nbf', decorations: { nbf } },
        { description: 'scope', decorations: { scope } },
        { description: 'all', decorations: { exp, nbf, scope } }
      ]
      changes.forEach(c => {
        describe(`replace ${c.description}`, function () {
          const expected = { ...cases.baseAccessTokenPayload, ...c.decorations }
          generateTests(c.decorations, expected)
        })
      })
    })
  })
  describe('standard exceptions', function () {
    const codes = ['invalid_request', 'invalid_scope']

    const description = 'a dummy error description'
    codes.forEach(c => {
      it(`correctly handles "${c}"`, async function () {
        const error = jwtError[c](description)
        decoratorImpl.rejects(error)
        await decoratePayload(
          this.logger,
          decorator,
          cases.authorizationCodeGrantRequest.request,
          cases.baseAccessTokenPayload
        ).should.be.rejectedWith(error)
        sinon.assert.calledOnce(decoratorImpl)
        const call = decoratorImpl.getCall(0)
        shouldBeExpectedRequest(cases.authorizationCodeGrantRequest.request, call.args[1])
        call.args[2].should.not.equal(cases.baseAccessTokenPayload)
        call.args[2].should.deepEqual(cases.baseAccessTokenPayload)
      })
    })
  })
  describe('flags illegal things as contract violations', function () {
    const changes = [`jti`, `sub`, `aud`, `iss`, `client_id`, `iat`, `token_use`]
    changes.forEach(c => {
      it(`refuses a change to ${c}`, async function () {
        decoratorImpl.resolves({ [c]: 'illegal' })
        await decoratePayload(
          this.logger,
          decorator,
          cases.authorizationCodeGrantRequest.request,
          cases.baseAccessTokenPayload
        ).should.be.rejectedWith(PostconditionViolation)
      })
    })
    const errors = ['invalid_client', 'invalid_grant', 'unauthorized_client', 'unsupported_grant_type']
    errors.forEach(e => {
      it(`refuses errors with code ${e}`, async function () {
        decoratorImpl.rejects(jwtError[e]('illegal'))
        await decoratePayload(
          this.logger,
          decorator,
          cases.authorizationCodeGrantRequest.request,
          cases.baseAccessTokenPayload
        ).should.be.rejectedWith(ExceptionConditionViolation)
      })
    })
    const bogusResolutions = [undefined, null, 4, true, false, 0, -1, '', 'a string', ['an array']]
    bogusResolutions.forEach(b => {
      it(`refuses ${b} as resolution`, async function () {
        decoratorImpl.resolves(b)
        await decoratePayload(
          this.logger,
          decorator,
          cases.authorizationCodeGrantRequest.request,
          cases.baseAccessTokenPayload
        ).should.be.rejectedWith(PostconditionViolation)
      })
    })
    it(`refuses implementation errors`, async function () {
      decoratorImpl.rejects(new Error('invalid error'))
      await decoratePayload(
        this.logger,
        decorator,
        cases.authorizationCodeGrantRequest.request,
        cases.baseAccessTokenPayload
      ).should.be.rejectedWith(ExceptionConditionViolation)
    })
  })
})
