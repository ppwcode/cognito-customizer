/* eslint-env mocha */

const payloadDecoratorContract = require('../lib/payloadDecoratorContract')
const PromiseContract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const jwtSpec = require('../lib/jwtSpec')

describe('15.payloadDecoratorContract', function () {
  it('is a PromiseContract', function () {
    payloadDecoratorContract.should.be.an.instanceOf(PromiseContract)
  })
  it('always checks postconditions', function () {
    payloadDecoratorContract.verify.should.be.true()
    payloadDecoratorContract.verifyPostconditions.should.be.true()
  })
  it('carries an array of allowed error codes', function () {
    payloadDecoratorContract.allowedErrorCodes.should.be.an.Array()
    payloadDecoratorContract.allowedErrorCodes.forEach(ec => {
      ec.should.be.a.String()
      Object.keys(jwtSpec.tokenRequestErrorStatusCodes).should.containEql(ec)
    })
  })
})
