const stsConfig = require('../config/sts')

module.exports = { userPoolTokenEndpoint: stsConfig.tokenEndpoint }
