/* eslint-env mocha */

const proxyquire = require('proxyquire')
const sinon = require('sinon')
const cases = require('../_cases')
const configuration = require('./_configuration')
const jwtError = require('../../lib/jwtError')
const flowLogger = require('../../lib/_util/flowLogger')

const stub = {
  // MUDO contract?
  authorization_code: sinon.stub(),
  refresh_token: sinon.stub(),
  client_credentials: sinon.stub()
}
const tokensResult = {
  access_token: 'THIS IS AN ACCESS TOKEN. REALLY. TRUST ME.',
  token_type: 'Bearer',
  expires_in: 300
}
const tokensProblem = jwtError.invalid_scope('dummy')

describe('20.userPool/20.getTokens', function () {
  beforeEach(function () {
    this.getTokens = proxyquire('../../lib/userPool/getTokens', {
      './getTokensWithAuthorizationCode': stub.authorization_code,
      './getTokensWithRefreshToken': stub.refresh_token,
      './getTokensWithClientCredentials': stub.client_credentials
    })
    this.getTokens.contract.verifyPostconditions = true
    this.logger = flowLogger(cases.sot, cases.flowId, 'trace')
  })

  afterEach(function () {
    this.getTokens.contract.verifyPostconditions = false
    Object.keys(stub).forEach(k => {
      stub[k].reset()
    })
  })

  cases.tokenRequests.forEach(tr => {
    describe(tr.description, function () {
      it(`calls the correct method when all is well`, async function () {
        const expectedFunction = stub[tr.description]
        expectedFunction.resolves(tokensResult)
        console.log('request:', tr.request)
        const result = await this.getTokens.call(null, configuration, this.logger, tr.request)
        console.log(result)
        console.log()
        sinon.assert.calledOnce(expectedFunction)
        const call = expectedFunction.getCall(0)
        call.args[0].should.equal(configuration)
        call.args[2].should.equal(tr.request)
        result.should.equal(tokensResult)
        Object.keys(stub)
          .filter(k => k !== tr.description)
          .forEach(k => sinon.assert.notCalled(stub[k]))
      })
      it(`rethrows problems`, async function () {
        const expectedFunction = stub[tr.description]
        expectedFunction.rejects(tokensProblem)
        console.log('request:', tr.request)
        await this.getTokens.call(null, configuration, this.logger, tr.request).should.be.rejectedWith(tokensProblem)
      })
    })
  })

  describe('illegal grant types', function () {
    async function testIllegalGrantType (getTokens, logger, grantType) {
      const request = {
        headers: {
          'content-type': 'application/x-www-form-urlencoded',
          accept: 'application/json',
          'x-flow-id': '973c6507-4c50-4d6f-bf4a-5b1e85eae998'
        },
        body: {}
      }
      if (grantType !== undefined) {
        request.body.grant_type = grantType
      }
      console.log('request:', request)
      const err = await getTokens(configuration, logger, request).should.be.rejectedWith(/unsupported_grant_type/)
      console.log(err)
      Object.keys(stub).forEach(k => sinon.assert.notCalled(stub[k]))
    }

    it('throws unsupported_grant_type when the request calls for an invalid grant type', async function () {
      await testIllegalGrantType(this.getTokens, this.logger, 'wacko type')
    })
    it('throws unsupported_grant_type when the request has an empty grant type', async function () {
      await testIllegalGrantType(this.getTokens, this.logger, '')
    })
    it('throws unsupported_grant_type when the request has a null grant type', async function () {
      await testIllegalGrantType(this.getTokens, this.logger, null)
    })
    it('throws unsupported_grant_type when the request has a number grant type', async function () {
      await testIllegalGrantType(this.getTokens, this.logger, 87)
    })
    it('throws unsupported_grant_type when the request has no grant type', async function () {
      await testIllegalGrantType(this.getTokens, this.logger)
    })
  })
})
