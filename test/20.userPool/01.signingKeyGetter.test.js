/* eslint-env mocha */

const signingKeyGetter = require('../../lib/userPool/signingKeyGetter')
const PromiseContract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const cases = require('../_cases')
const { JwksError, SigningKeyNotFoundError } = require('jwks-rsa')

// noinspection SpellCheckingInspection
const goodKid = '/Lwlox8XyhOHLPBX+YUZJqmOJzW9X/rioMhfWaP1oqg='

describe('28.userPoolSigningKeyGetter', function () {
  beforeEach(function () {
    signingKeyGetter.contract.verifyPostconditions = true
  })

  afterEach(function () {
    signingKeyGetter.contract.verifyPostconditions = false
  })

  it(`carries the contract for the getter`, function () {
    signingKeyGetter.getSigningKey.should.be.an.instanceOf(PromiseContract)
  })
  it(`returns a contract function for a jwksUri`, function () {
    const result = signingKeyGetter(cases.jwksUri)
    console.log(result)
    console.log()
  })
  describe('generated getter, good jwksUri', function () {
    before(function () {
      this.getter = signingKeyGetter(cases.jwksUri)
    })

    beforeEach(function () {
      this.getter.contract.verifyPostconditions = true
    })

    afterEach(function () {
      this.getter.contract.verifyPostconditions = false
    })

    it(`returns a public key for a good jwksUri and a good kid`, async function () {
      // use call, because this is the test, and that crashes the ExceptionViolation report
      const result = await this.getter.call(null, goodKid)
      console.log(result)
      console.log()
      result.should.be.a.String()
      result.should.not.be.empty()
    })
    it(`throws with a good jwksUri and a bad kid`, async function () {
      // use call, because this is the test, and that crashes the ExceptionViolation report
      const err = await this.getter.call(null, 'bad kid').should.be.rejectedWith(SigningKeyNotFoundError)
      console.log(err)
      console.log()
    })
  })
  describe('generated getter, bad jwksUri', function () {
    before(function () {
      this.getter = signingKeyGetter('https://example.org/there/is/no/token/here')
    })

    beforeEach(function () {
      this.getter.contract.verifyPostconditions = true
    })

    afterEach(function () {
      this.getter.contract.verifyPostconditions = false
    })

    it(`throws with a bad jwksUri and a good kid`, async function () {
      // use call, because this is the test, and that crashes the ExceptionViolation report
      const err = await this.getter.call(null, goodKid).should.be.rejectedWith(JwksError)
      console.log(err)
      console.log()
    })
    // NOTE: extensive testing might trigger JwksRateLimitError
    // NOTE: difficult to test JwksRateLimitError; not done for now
    /* IDEA: Test JwksRateLimitError with proxyquire on jwks-rsa. But that would be difficult, because of the complex
             structure of jwks-rsa, via the client. */
  })
})
