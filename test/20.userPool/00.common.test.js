/* eslint-env mocha */

const common = require('../../lib/userPool/common')

function shouldBeSchema (schema) {
  schema.should.be.an.Object()
  schema.isJoi.should.be.true()
}

function shouldBeConditions (conditions) {
  conditions.should.be.an.Array()
  conditions.forEach(c => c.should.be.a.Function())
}

describe('20.userPool/00.commonConditions', function () {
  it('is a Joi schema', function () {
    common.should.be.an.Object()
    shouldBeSchema(common.configurationSchema)
    shouldBeSchema(common.tokensSchema)
    shouldBeConditions(common.pre)
    shouldBeConditions(common.post)
    shouldBeConditions(common.exception)
    common.mandatoryBodyParam.should.be.a.Function()
  })
  describe('mandatoryBodyParam', function () {
    it('does not throw when the param does exist')
    it('throws when the param does not exist')
    it('throws when the param does not exist, and has the extra message')
  })
})
