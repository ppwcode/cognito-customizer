/* eslint-env mocha */

const getTokens = require('../../lib/userPool/getTokensWithClientCredentials')
const cases = require('../_cases')
const Hoek = require('hoek')
const configuration = require('./_configuration')
const flowLogger = require('../../lib/_util/flowLogger')

async function fails (logger, request, expectedMessageRegex) {
  console.log('request:', request)
  const error = await getTokens(configuration, logger, request).should.be.rejectedWith(expectedMessageRegex)
  console.log(error)
  console.log()
}

describe('20.userPool/11.getTokensWithClientCredentials', function () {
  beforeEach(function () {
    getTokens.contract.verifyPostconditions = true
    this.logger = flowLogger(cases.sot, cases.flowId, 'trace')
  })

  afterEach(function () {
    getTokens.contract.verifyPostconditions = false
  })

  it(`returns a token set from the User Pool when all is well, with the exact scopes as defined, and extra body parameters`, async function () {
    const request = Hoek.clone(cases.clientCredentialsGrantRequest.request)
    request.body.someUnusedBodyString = 'some unused body string'
    request.body.someUnusedBodyNumber = 98
    request.body.someUnusedBodyBoolean1 = true
    request.body.someUnusedBodyBoolean2 = false
    request.body.someUnusedBodyArray = [78, 'array element']
    request.body.someUnusedBodyObject = { nestedProperty: 'nested property' }
    request.body.someUnusedBodyUndefined = undefined
    request.body.someUnusedBodyNull = null
    console.log('request:', request)
    const result = await getTokens(configuration, this.logger, request)
    console.log(result)
    console.log()
  })
  it(`returns a token set from the User Pool when all is well, without scope`, async function () {
    const request = Hoek.clone(cases.clientCredentialsGrantRequest.request)
    delete request.body.scope
    console.log('request:', request)
    const result = await getTokens(configuration, this.logger, request)
    console.log(result)
    console.log()
  })
  it(`throws without Authorization header`, async function () {
    const request = Hoek.clone(cases.clientCredentialsGrantRequest.request)
    delete request.headers.authorization
    await fails(this.logger, request, /invalid_client: no client credentials found in Authorization header/)
  })
  it(`throws with credentials without client id`, async function () {
    const request = Hoek.clone(cases.clientCredentialsGrantRequest.request)
    request.headers.authorization = cases.auth(undefined, 'some secret')
    await fails(this.logger, request, /invalid_client: no client credentials found in Authorization header/)
  })
  it(`throws with credentials without client secret`, async function () {
    const request = Hoek.clone(cases.clientCredentialsGrantRequest.request)
    request.headers.authorization = cases.auth('some id', undefined)
    await fails(this.logger, request, /invalid_client: no client credentials found in Authorization header/)
  })
  it(`throws with malformed credentials`, async function () {
    const request = Hoek.clone(cases.clientCredentialsGrantRequest.request)
    request.headers.authorization = 'this is not the format we need'
    await fails(this.logger, request, /invalid_client: no client credentials found in Authorization header/)
  })
  it(`throws with bad credentials`, async function () {
    const request = Hoek.clone(cases.clientCredentialsGrantRequest.request)
    request.headers.authorization = cases.auth(cases.clientId, 'a wrong secret')
    await fails(this.logger, request, /invalid_client: user pool denied access/)
  })
  it(`throws when other scopes are requested then configured for the client`, async function () {
    const request = Hoek.clone(cases.clientCredentialsGrantRequest.request)
    request.body.scope = 'someScope someOtherScope'
    await fails(this.logger, request, /invalid_grant: user pool denied access/) // would expect invalid_scope, although it does not exist
  })
  it(`throws when more scopes are requested than configured for the client`, async function () {
    const request = Hoek.clone(cases.clientCredentialsGrantRequest.request)
    request.body.scope += ' someOtherScope'
    await fails(this.logger, request, /invalid_grant: user pool denied access/) // would expect invalid_scope, although it does not exist
  })
})
