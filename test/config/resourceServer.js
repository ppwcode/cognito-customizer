const terraformOutputs = require('./terraformOutputs')

const tfOutput = terraformOutputs['I-resource_server-recon'].value

// noinspection JSUnresolvedVariable
/**
 * Configuration options of the resource server used by this client.
 *
 * @module
 */
module.exports = {
  /**
   * String identifying the resource server in the STS.
   *
   * @type {string}
   */
  audience: tfOutput.identifier,

  /**
   * URI of the token endpoint of the STS
   *
   * @type {Array<string>}
   */
  scopes: tfOutput.scope_identifiers
}
