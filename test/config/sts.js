const terraformOutputs = require('./terraformOutputs')

/**
 * Configuration options of the Secure Token Service (STS) used by this client.
 *
 * @module
 */
module.exports = {
  /**
   * Identification of the STS as token issuer.
   *
   * @type {string}
   */
  iss: terraformOutputs['I-user_pool'].value.endpoint,

  /**
   * URI of the token endpoint of the STS
   *
   * @type {string}
   */
  tokenEndpoint: terraformOutputs['I-uri'].value.client_credentials_grant,

  /**
   * URI of the certificate endpoint of the STS
   *
   * @type {string}
   */
  certificateEndpoint: terraformOutputs['I-user_pool'].value.certificate_endpoint
}
