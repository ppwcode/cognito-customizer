/**
 * @typedef {object} StsUris
 * @property {string} client_credentials_grant
 */

/**
 * @typedef {object} UserPool
 * @property {string} endpoint
 * @property {string} certificate_endpoint
 */

/**
 * @typedef {object} ResourceServerOptions
 * @property {Array<string>} scope_identifiers
 */

/**
 * @typedef {object} ClientMachineOptions
 * @property {string} id
 * @property {string} secret
 */

/**
 * @typedef {object} TfStateModuleOutput
 * @property {StsUris} I-uri.value
 * @property {UserPool} I-user_pool.value
 * @property {ResourceServerOptions} I-resource_server-recon.value
 * @property {ClientMachineOptions} I-client-machine.value
 */

/**
 * @typedef {object} TfStateModule
 * @property {TfStateModuleOutput} outputs
 */

/**
 * @typedef {object} TfState
 * @property {Array<TfStateModule>} modules
 */

/**
 * @type {TfState}
 */
const terraformState = require('./tfstate')

/**
 * The Terraform outputs of the Terraform configuration `../../terraform/` from `../tfstate.json`.
 *
 * To generate `../tfstate.json` do `npm run tfstate`.
 *
 * @module
 * @type {TfStateModuleOutput}
 */
module.exports = terraformState.modules[0].outputs
