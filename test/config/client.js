const terraformOutputs = require('./terraformOutputs')

const tfOutput = terraformOutputs['I-client-machine'].value

/**
 * Configuration options of this client.
 *
 * @module
 */
module.exports = {
  /**
   * Client ID of this client
   *
   * @type {string}
   */
  id: tfOutput.id,

  /**
   * Client secret of this client
   *
   * @type {string}
   */
  secret: tfOutput.secret
}
