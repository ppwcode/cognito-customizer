/* eslint-env mocha */

const signJwt = require('../lib/signJwt')
const cases = require('./_cases')
const jwtVerify = require('util').promisify(require('jsonwebtoken').verify)
// noinspection NpmUsedModulesInstalled
const { performance } = require('perf_hooks')
const depromisify = require('../lib/_util/depromisify')
const flowLogger = require('../lib/_util/flowLogger')

async function ensurePublicKey (header) {
  header.should.be.an.Object()
  header.kid.should.equal(cases.keypair.id)
  console.log('public key:')
  console.log(cases.keypair.publicKey)
  return Promise.resolve(cases.keypair.publicKey)
}

const timerMarkBefore = 'before signJwt'
const timerMarkAfter = 'after signJwt'
const timerMeasure = 'signJwt'
const timerMarkBeforeBulk = 'before signJwt Bulk'
const timerMarkAfterBulk = 'after signJwt Bulk'
const timerMeasureBulk = 'signJwt Bulk'
const speedTries = 100
const maxMs = 10

describe('80.signJwt', function () {
  beforeEach(function () {
    signJwt.contract.verifyPostconditions = true
    this.logger = flowLogger(cases.sot, cases.flowId, 'trace')
    this.loggerFlushed = this.logger.flushed
  })

  afterEach(function () {
    signJwt.contract.verifyPostconditions = false
  })

  it('generates a bearer access token', async function () {
    console.log('payload before signing:')
    console.log(cases.accessTokenPayload)
    console.log()
    performance.mark(timerMarkBefore)
    const result = await signJwt(this.logger, cases.accessTokenPayload, cases.keypair)
    performance.mark(timerMarkAfter)
    console.log('result:')
    console.log(result)
    console.log('expires in seconds:', cases.expiresInSeconds)
    const verificationTime = cases.accessTokenPayloadParams.iat + cases.expiresInSeconds - 1
    console.log('verification time:', verificationTime)
    console.log()
    const decoded = await jwtVerify(result.token, depromisify(ensurePublicKey), {
      algorithms: ['RS256'],
      audience: cases.audience,
      issuer: cases.issuer,
      subject: cases.subject,
      jwtid: cases.accessTokenId,
      complete: true,
      clockTimestamp: verificationTime
    })
    console.log('decoded:')
    console.log(decoded)
    console.log()
    decoded.payload.should.eql(cases.rehydratedAccessTokenPayload)
    performance.measure(timerMeasure, timerMarkBefore, timerMarkAfter)
    console.log(`duration: ${performance.getEntriesByName(timerMeasure)[0].duration}ms`)
    console.log()
  })
  it('is fast', async function () {
    const logger = this.logger.start('signJwt#isFast', {})
    const promises = []
    performance.mark(timerMarkBeforeBulk)
    for (let i = speedTries; i > 0; i--) {
      promises.push(signJwt(logger.concurrent(`i${i}`), { ...cases.accessTokenPayload, i }, cases.keypair))
    }
    const results = await Promise.all(promises)
    performance.mark(timerMarkAfterBulk)
    performance.measure(timerMeasureBulk, timerMarkBeforeBulk, timerMarkAfterBulk)
    const duration = performance.getEntriesByName(timerMeasureBulk)[0].duration / speedTries
    console.log(`duration: ${duration}ms`)
    results.forEach(r => {
      console.log(r.token)
    })
    console.log()
    duration.should.be.belowOrEqual(maxMs)
    logger.nominal()
  })
})
