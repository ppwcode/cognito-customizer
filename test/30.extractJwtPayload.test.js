/* eslint-env mocha */

const extractJwtPayload = require('../lib/extractJwtPayload')
const sign = require('util').promisify(require('jsonwebtoken').sign)
const cases = require('./_cases')
const jwtSpec = require('../lib/jwtSpec')
const createUserPoolSigningKeyGetter = require('../lib/userPool/signingKeyGetter')
const getUserPoolSigningKey = createUserPoolSigningKeyGetter.getSigningKey
const ClientOAuth2 = require('client-oauth2')
const stsConfig = require('./config/sts')
const resourceServerConfig = require('./config/resourceServer')
const clientConfig = require('./config/client')
const flowLogger = require('../lib/_util/flowLogger')

const userPool = new ClientOAuth2({
  clientId: clientConfig.id,
  clientSecret: clientConfig.secret,
  accessTokenUri: stsConfig.tokenEndpoint,
  scopes: resourceServerConfig.scopes,
  body: {
    audience: resourceServerConfig.audience
  }
})

describe('30.extractJwtPayload', function () {
  beforeEach(function () {
    extractJwtPayload.contract.verifyPostconditions = true
    this.logger = flowLogger(cases.sot, cases.flowId, 'trace')
  })

  afterEach(function () {
    extractJwtPayload.contract.verifyPostconditions = false
  })

  it(`extracts the payload from a forged JWT token correctly`, async function () {
    const token = await sign(cases.userPoolAccessTokenPayload, cases.keypair.privateKey, {
      algorithm: jwtSpec.algorithm,
      keyid: cases.keypair.id
    })
    const userPoolSigningKeyGetter = getUserPoolSigningKey.implementation(async function (kid) {
      cases.keypair.id.should.equal(kid)
      return Promise.resolve(cases.keypair.publicKey)
    })
    console.log('token:')
    console.log(token)
    console.log()
    const result = await extractJwtPayload(this.logger, userPoolSigningKeyGetter, cases.sot, token)
    console.log(result)
    console.log()
    result.should.deepEqual(cases.userPoolAccessTokenPayload)
  })
  it(`extracts the payload from a real JWT token from a User Pool`, async function () {
    const userPoolSigningKeyGetter = createUserPoolSigningKeyGetter(stsConfig.certificateEndpoint)
    const userPoolResponse = await userPool.credentials.getToken()
    console.log('token response:')
    console.log(userPoolResponse.data)
    console.log()
    const userPoolAccessToken = userPoolResponse.accessToken
    console.log('userPoolAccessToken:')
    console.log(userPoolAccessToken)
    console.log()
    const result = await extractJwtPayload(this.logger, userPoolSigningKeyGetter, cases.sot, userPoolAccessToken)
    console.log(result)
    console.log()
    result.should.containEql({
      sub: clientConfig.id,
      token_use: 'access',
      scope: resourceServerConfig.scopes.join(' '),
      iss: stsConfig.iss,
      client_id: clientConfig.id
    })
  })
})
