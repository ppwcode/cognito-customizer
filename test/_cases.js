const date2SecSinceEpoch = require('../lib/_util/date2SecSinceEpoch')
const { certificateEndpoint } = require('./config/sts')
const client = require('./config/client')
const resourceServer = require('./config/resourceServer')
const Hoek = require('hoek')

/**
 * Support base64 in node like how it works in the browser.
 *
 * @param  {string} string
 * @return {string}
 */
function btoa (string) {
  return Buffer.from(string).toString('base64')
}

/**
 * Ensure a value is a string.
 *
 * @param  {string} str
 * @return {string}
 */
function toString (str) {
  return str == null ? '' : String(str)
}

/**
 * Create basic auth header.
 *
 * @param  {string} username
 * @param  {string} password
 * @return {string}
 */
function auth (username, password) {
  return 'Basic ' + btoa(toString(username) + ':' + toString(password))
}

const accessTokenId = '020d4e05-ed8b-480d-b2f9-efeb5d7d65e7'
const sot = new Date(2019, 3, 8, 11, 3, 24, 481)
const jwksUri = certificateEndpoint
const clientId = client.id
const codeGrant = 'DUMMY CODE GRANT'
const oldRefreshToken = 'DUMMY OLD REFRESH TOKEN'
const scopes = ['dummy:scope1', 'dummy:scope2']
const scopeString = scopes.join(' ')
const audience = 'https://audience.example.org/some.service'
const flowId = '973c6507-4c50-4d6f-bf4a-5b1e85eae998'

/* emulate Cognito token endpoint more or less
   https://docs.aws.amazon.com/cognito/latest/developerguide/token-endpoint.html */
const headers = {
  authorization: auth(client.id, client.secret), // NOTE: optional, required in client_credentials
  'content-type': 'application/x-www-form-urlencoded', // TODO although in Lambda it will always be application/json?
  accept: 'application/json',
  'x-flow-id': flowId,
  'x-some-other-header': 'some other header value'
}
// noinspection SpellCheckingInspection
const userPoolAccessTokenPayload = {
  sub: 'qudaeu7d3elehals9mdtpk3ek',
  token_use: 'access',
  scope: 'https://ppwcode.org/cognito-customizer/dummy-service/read:data',
  auth_time: 1554893329,
  iss: 'https://cognito-idp.eu-west-1.amazonaws.com/eu-west-1_RKfawX0rG',
  exp: 1554896929,
  iat: 1554893329,
  version: 2,
  jti: '2769bb35-e72c-417e-85f6-b5787283a033',
  client_id: 'qudaeu7d3elehals9mdtpk3ek',
  aud: audience
}
// noinspection SpellCheckingInspection
const userPoolAccessToken =
  'eyJraWQiOiJcL0x3bG94OFh5aE9ITFBCWCtZVVpKcW1PSnpXOVhcL3Jpb01oZldhUDFvcWc9IiwiYWxnIjoiUlMyNTYifQ.eyJzdWIiOiJxdWRhZXU3ZDNlbGVoYWxzOW1kdHBrM2VrIiwidG9rZW5fdXNlIjoiYWNjZXNzIiwic2NvcGUiOiJodHRwczpcL1wvcGljdG9wZXJmZWN0LmNvbVwvcGljdG9wZXJmZWN0LXJlY29uXC9yZWFkOmRhdGEiLCJhdXRoX3RpbWUiOjE1NTQ4OTMzMjksImlzcyI6Imh0dHBzOlwvXC9jb2duaXRvLWlkcC5ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVwvZXUtd2VzdC0xX1JLZmF3WDByRyIsImV4cCI6MTU1NDg5NjkyOSwiaWF0IjoxNTU0ODkzMzI5LCJ2ZXJzaW9uIjoyLCJqdGkiOiIyNzY5YmIzNS1lNzJjLTQxN2UtODVmNi1iNTc4NzI4M2EwMzMiLCJjbGllbnRfaWQiOiJxdWRhZXU3ZDNlbGVoYWxzOW1kdHBrM2VrIn0.bAcxHCY18Kj7BLc9n03BQzdQeuGxRo0vRLuf3TGLcYI7cus5H9NA18PaAKWeNepk7WxT1-VGfdmlRNKUKTVcNXV84OJ5Vk1VWGZcy8144lps5-KuO3A25asMGCS3eVvF6HskMu8S58WdnWeyHGR5n4TZ_S1EW--hC4KL8FiYa6xgf2wu60I0150KumsJW6x36qPY5EBonekIqrvLC5jbJbiE5U_l0HfMuKym0899ixKe6mNQt_5Gq8ewWybunBIoqEGmMytlmL8rW6H9Rac2vGztEFODKo7JVHBrlxYXG2IlvMXTqKmB20lk8z5cFeIPKTGo_7MTkUbDAOclwN6YGA'
const userPoolResponse = {
  access_token: userPoolAccessToken,
  expires_in: 3600,
  token_type: 'Bearer'
}

const newRefreshToken = 'DUMMY NEW REFRESH TOKEN'

const authorizationCodeGrantRequest = {
  description: 'authorization_code',
  request: {
    headers: headers,
    body: {
      grant_type: 'authorization_code',
      code: codeGrant,
      code_verifier: 'DUMMY CODE VERIFIER', // required, because we only support PKCE
      redirect_uri: 'https://example.com/loggedin.html',
      client_id: clientId,
      // scope makes no sense in authorization code grant
      extraBodyParameter: 'THIS IS AN EXTRA BODY PARAMETER'
    }
  },
  userPoolResponse: { ...Hoek.clone(userPoolResponse), refresh_token: newRefreshToken }
}

const refreshTokenGrantRequest = {
  description: 'refresh_token',
  request: {
    headers: headers,
    body: {
      grant_type: 'refresh_token',
      refresh_token: oldRefreshToken,
      client_id: clientId,
      scope: scopeString, // optional, cognito does not use this for refresh token grants
      extraBodyParameter: 'THIS IS AN EXTRA BODY PARAMETER'
    }
  },
  userPoolResponse: Hoek.clone(userPoolResponse)
}

const clientCredentialsGrantRequest = {
  description: 'client_credentials',
  request: {
    headers: headers,
    body: {
      grant_type: 'client_credentials',
      // client_id in authorization header, optional here
      // client_secret in authorization header
      scope: resourceServer.scopes.join(' '), // optional
      extraBodyParameter: 'THIS IS AN EXTRA BODY PARAMETER'
    }
  },
  userPoolResponse: Hoek.clone(userPoolResponse)
}

const tokenRequests = [authorizationCodeGrantRequest, refreshTokenGrantRequest, clientCredentialsGrantRequest]

const issuedAtSecSinceEpoch = date2SecSinceEpoch(sot)
const expiresInSeconds = 456
const expirySecSinceEpoch = issuedAtSecSinceEpoch + expiresInSeconds
const issuer = 'https://api.ppwcode.org/sts'
const subject = 'DUMMY SUBJECT'
const commonAccessTokenPayload = {
  jti: accessTokenId,
  iss: issuer,
  client_id: clientId,
  sub: subject,
  aud: audience,
  scope: scopeString,
  iat: issuedAtSecSinceEpoch
}
const baseAccessTokenPayload = {
  ...commonAccessTokenPayload,
  token_use: 'access',
  exp: expirySecSinceEpoch,
  nbf: issuedAtSecSinceEpoch
}
const extraAccessTokenClaims = {
  extraStringClaim: 'extra string claim',
  extraNumberClaim: 456,
  extraBooleanClaim: false,
  extraDateClaim: sot,
  extraArrayClaim: ['string in array', 678, { what: 'object in array' }, [0, 1, 2], true],
  extraObjectClaim: {
    what: 'extra object claim',
    number: 123,
    array: [4, 5, 6],
    object: {
      what: 'nested'
    }
  },
  extra_claim_with_underscores: 'extra claim with underscores',
  'https://ppwcode.org/jwt/sts/extra-claim': 'extra claim in URI form'
}
const accessTokenPayloadParams = {
  ...commonAccessTokenPayload,
  ...extraAccessTokenClaims
}
const accessTokenPayload = {
  ...accessTokenPayloadParams,
  token_use: 'access',
  exp: expirySecSinceEpoch,
  nbf: issuedAtSecSinceEpoch
}
const rehydratedAccessTokenPayload = {
  ...accessTokenPayloadParams,
  extraDateClaim: accessTokenPayloadParams.extraDateClaim.toISOString(),
  token_use: 'access',
  exp: expirySecSinceEpoch,
  nbf: issuedAtSecSinceEpoch
}

// noinspection SpellCheckingInspection
const keypair = {
  id: '708c8e8f-67c5-44f3-b98a-26537dfd0d5a',
  publicKey:
    '-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAjuaPI4gWnkbQH3JKNtTSfkdP36FZuOVC6SQITuNb4nUi0fb2BKn4Ik6LJikz\nxhvh3EjbksgXnmv1LEA1y5vZE4z8/OVJLkw+oHGrvhRhzNiCNni+a6ufIcBlyO+ETH3j5bK2\n+5St4l9ett7amW7jKfjvC7hxmPBlN/DMSgdSJS3/+U71UMZbxL3SHvYoSewe8ykdlfLph43X\nnCe3Le+Ih/CuIMDI5c4RD2bCFDQYtwJZGyRUC0K7HtsnWYjEpHLPoh7DEUM4C2w8nHLxiMno\nG2rfYsC64c7M1LDbUP0BUgaZfkOffS+s8Qfnhd33QlVANGdUyQxjqMgimS8RCcnTBQIDAQAB\n-----END RSA PUBLIC KEY-----\n',
  privateKey:
    '-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAjuaPI4gWnkbQH3JKNtTSfkdP36FZuOVC6SQITuNb4nUi0fb2BKn4Ik6L\nJikzxhvh3EjbksgXnmv1LEA1y5vZE4z8/OVJLkw+oHGrvhRhzNiCNni+a6ufIcBlyO+ETH3j\n5bK2+5St4l9ett7amW7jKfjvC7hxmPBlN/DMSgdSJS3/+U71UMZbxL3SHvYoSewe8ykdlfLp\nh43XnCe3Le+Ih/CuIMDI5c4RD2bCFDQYtwJZGyRUC0K7HtsnWYjEpHLPoh7DEUM4C2w8nHLx\niMnoG2rfYsC64c7M1LDbUP0BUgaZfkOffS+s8Qfnhd33QlVANGdUyQxjqMgimS8RCcnTBQID\nAQABAoIBAQCOGbNpL+DeD6jSPIKUN9oBfMRuqzJfbIu27v/cArbSYIz5oc1PIf3/j39LuVkk\nvYFB3qmKMrNZ9BzfnhJgoF+i02aXzSGSinsUbTTNVdNTMlF5/WPOCeG6XGpa/+Lddap0Nd1E\nG7s2CoRS8RUL0nrOuB5t10IPRa5BjJB5ZQJIuRLJt5T82693od85TZuUg2TKACBYUfFOVqLN\nfNDDBI6coMoaIiF8fEJx0mCudLSLWi6MT7DLS7pWbK6CtsPAhAuydYQ9N0/F3kfOaT2o+QEx\nP39TRLp5pN6z95DRGsSanJ39l5FvvDH6XvO1dyotKDLDtvOgEuxNnsEWUyko99lZAoGBAMcL\n5zIBG0TDvvX/2JVxXOgiYtOWS3paw4/BHB8weZBgjXcnj2+eYS3IUJQyYGPVJk15t+lbPJIa\ndm2HNRzESOfgyXVemRvtBQ8mAEbB9pc73Y3k81YbcF9QgAz4VZ4KrNd6NskbWllTfpw5s8Eh\n8L4MnPPfUlnEd6VH4d2dP5gPAoGBALfJ/MGez5DT2PkZXobTQKcyDHH0VPWivvqOaGhvo+Qt\npMHy9jUbF8KkWLB3sJOoTQdCrrpi+34h7e8NCE+wzma4WT6iZUzAwTyLUXU6DWO8RC5azUzl\n3BrZKgXdW4cdNEfVKDyGkDUVUZ2db4eWiQp2wY72ic6O8WChNTGd06+rAoGAFqIH2+u8Sglo\nAVjiK7wEMHEYg66nTnZbnlD6/aDpcb1I2K+q46pCqo4Ie5Fu3Gs0O8MHYoV5UbOom7OwGmFH\nWPZ6cdob1s0QsjRD+8e6Xl/0RVovQS9Fi9D/fnoOYjYciTPgXuW5VEbmMqVtBxzw3utYPXK7\nTnHEmQqfaC+lN4kCgYBwfQBmwXzpd8BlNlHU05fOvaNAW/tkHKgnUuI0iaAWGJmu27Y16s8w\nIsblu1woA+qhdv7atZqYKMwodCGSJPUvicWVwG+f+ppLJDpHNbDcSm6wsILXfhipliFSVZmf\nqRBGpwBpizLYSqQZhDKvGkPmU6lEh0DxXCwAqKqWI7Nc4QKBgBcl/X6x63dqAnGv73/v/THC\n8wOg85UC5+ZxkJU4HwDcAQlD4rV80q0kwEcySjL+bGf8ntidD3S/YfcNz8nfM+PZVZFwZ4jP\nMUi4pznVBjsG3nQBGcRbpOJpAC3WalbFPivRl3Q1CVYoHSBtpJ3Bi8yk9VmF64zqVA0+gJ3F\nlATX\n-----END RSA PRIVATE KEY-----\n'
}

module.exports = {
  jwksUri,
  sot,
  codeGrant,
  oldRefreshToken,
  auth,
  authorizationCodeGrantRequest,
  refreshTokenGrantRequest,
  clientCredentialsGrantRequest,
  tokenRequests,
  newRefreshToken,
  userPoolAccessTokenPayload,
  userPoolAccessToken,
  userPoolResponse,
  accessTokenId,
  issuedAtSecSinceEpoch,
  expiresInSeconds,
  expirySecSinceEpoch,
  issuer,
  clientId,
  subject,
  audience,
  scopes,
  flowId,
  commonAccessTokenPayload,
  baseAccessTokenPayload,
  extraAccessTokenClaims,
  accessTokenPayloadParams,
  accessTokenPayload,
  rehydratedAccessTokenPayload,
  keypair
}
