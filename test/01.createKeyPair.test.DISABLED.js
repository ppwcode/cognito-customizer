/* eslint-env mocha */

/* NOTE: https://github.com/juliangruber/keypair is used, because crypto.generateKeyPair is only available since Node
         10. */
const keypair = require('keypair')
const NodeRSA = require('node-rsa')
const bits = 2048
const times = 10

function time (generator) {
  let pair
  const start = Date.now()
  for (let i = times; i > 0; i--) {
    pair = generator()
    console.log('.')
  }
  const end = Date.now()
  console.log(pair)
  console.log(`Duration: ${(end - start) / (1000 * times)}`)
}

describe('create a keypair', function () {
  this.timeout(1000000)

  it('can create a keypair', function () {
    time(() => keypair({ bits: bits }))
  })
  it('can create a keypair too', function () {
    const rsa = new NodeRSA()
    time(() => {
      rsa.generateKeyPair(bits, 65537)
      return {
        public: rsa.exportKey('pkcs1-public-pem'),
        private: rsa.exportKey('pkcs1-private-pem')
      }
    })
  })
})
