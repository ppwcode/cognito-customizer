/* eslint-env mocha */

const date2SecSinceEpoch = require('../../lib/_util/date2SecSinceEpoch')
const cases = require('../_cases')

const dateWithMs = cases.sot
const dateWithoutMs = new Date(
  cases.sot.getFullYear(),
  cases.sot.getMonth(),
  cases.sot.getDate(),
  cases.sot.getHours(),
  cases.sot.getMinutes(),
  cases.sot.getSeconds(),
  0
)

function test (date) {
  const result = date2SecSinceEpoch(date)
  console.log(`${date} -> ${result} (${date.getTime()})`)
  const reversed = new Date(result * 1000)
  console.log(`${result} -> ${reversed}`)
  console.log()
  reversed.getTime().should.equal(dateWithoutMs.getTime())
}

describe('date2SecSinceEpoch', function () {
  beforeEach(function () {
    date2SecSinceEpoch.contract.verifyPostconditions = true
  })

  afterEach(function () {
    date2SecSinceEpoch.contract.verifyPostconditions = false
  })

  it('works for a date with ms', function () {
    return test(dateWithMs)
  })
  it('works for a date without ms', function () {
    return test(dateWithoutMs)
  })
})
