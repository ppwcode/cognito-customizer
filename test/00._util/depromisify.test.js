/* eslint-env mocha */

const depromisify = require('../../lib/_util/depromisify')
const should = require('should')

const expected = 'this is the expected result'

function testNominal (func, done) {
  const withCallback = depromisify(func)
  withCallback((err, result) => {
    try {
      console.log(result)
      should(err).not.be.ok()
      result.should.equal(expected)
      done()
    } catch (failure) {
      done(failure)
    }
  })
}

function testException (func, done) {
  const withCallback = depromisify(func)
  withCallback((err, result) => {
    try {
      console.log(err)
      should(result).be.undefined()
      err.should.equal(expected)
      done()
    } catch (failure) {
      done(failure)
    }
  })
}

describe('depromisify', function () {
  it('works for a fast nominal return', function (done) {
    async function returns () {
      return expected
    }

    testNominal(returns, done)
  })
  it('works for a nominal resolution', function (done) {
    function resolves () {
      return Promise.resolve(expected)
    }

    testNominal(resolves, done)
  })
  it('works for a fast exception', function (done) {
    async function throwing () {
      throw expected
    }

    testException(throwing, done)
  })
  it('works for a rejection', function (done) {
    function rejects () {
      return Promise.reject(expected)
    }

    testException(rejects, done)
  })
})
