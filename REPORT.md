Deployed the Lambda successfully with `sls deploy`.

`sls deploy function` _overwrites_ the deployed code, and does not create a new version ??!?!

Coupled the Lambda to the UserPool trigger by hand.

Called from doctored existing Dojo UI:

- extra claims do appear in id token
- extra claims do not appear in access token

See [`temp.json`](temp.json)

Called with `client-machine`: is not triggered. This is also an access token.

Do we have to use other triggers?

This is a major problem. Unless we use the id token as access token.

**TODO** Still need to check whether we get extra request parameters.

FOUND the following thread on the forums d.d 2017-11-03 - 2019-03-08:
https://forums.aws.amazon.com/thread.jspa?threadID=266827

Another post lists similar and more limitations: https://forums.aws.amazon.com/thread.jspa?threadID=299448

https://forums.aws.amazon.com/thread.jspa?threadID=238381 lists an important request which was promised priority d.d
2016-08-30. NO progress yet.

No arrays in tokens either: https://forums.aws.amazon.com/thread.jspa?threadID=275934

Also, as encountered ourselves, the `PreTokenGeneration` trigger is not supported in CloudFormation. This thread
https://forums.aws.amazon.com/thread.jspa?threadID=268907 is over a year old.

So, this is an IdP, not an STS, is it?

About the extra request parameters:

This can only make sense in the `PreTokenGeneration` trigger. This is an example event we get:

    {
      version: '1',
      triggerSource: 'TokenGeneration_RefreshTokens',
      region: 'eu-west-1',
      userPoolId: 'eu-west-1_RKfawX0rG',
      userName: 'Google_108563704911276723208',
      callerContext: {
        awsSdkVersion: 'aws-sdk-unknown-unknown',
        clientId: 'sv198a432qopj5maevkuda6ul'
      },
      request: {
        userAttributes: {
          sub: 'ad728fab-10ff-4c2a-ab05-df0c89751dd7',
          email_verified: 'true',
          identities: '[{"userId":"108563704911276723208","providerName":"Google","providerType":"Google","issuer":null,"primary":true,"dateCreated":1550475457663}]',
          'cognito:user_status': 'EXTERNAL_PROVIDER',
          name: 'Jan Dockx',
          given_name: 'Jan',
          locale: 'nl',
          picture: 'https://lh4.googleusercontent.com/-ddoZa4JWLy8/AAAAAAAAAAI/AAAAAAAAZ2Q/WoiQ7ybsnFA/s96-c/photo.jpg',
          email: 'jandockx@gmail.com'
        },
        groupConfiguration: {
          groupsToOverride: [Array],
          iamRolesToOverride: [],
          preferredRole: null
        }
      },
      response: {claimsOverrideDetails: null}
    }

There is **no** place where extra request parameters van be offered. So this is not supported either.

## Alternatives

Of course there is Auth0.

[OneLogin](https://www.onelogin.com/product/pricing) does not seem a good alternative. There are only prices for €2
/user . month, and this seems aimed at something else.

[Okta](https://www.okta.com/pricing/) seems to be an alternative, under the _free_ development license. (for up to 100
MAUs, only for OAuth2). But, no "Add-on products available". It does mention "Customizable scopes and claims", but not
whether that can be done dynamically.

With Okta, we need a Lambda behind an API Gateway to customize tokens:
https://developer.okta.com/use_cases/inline_hooks/token_hook/token_hook/ This can add claims, and gets the user
(`data.context.user.id`) and the original request details (`data.context.request.url`) and information about the client,
issuer, etc. With client credentials, this is a `POST`, and the example does not show that, but it does show that
"everything" is available.

Starting point is https://developer.okta.com/

## Choices

Auth0 is very comparable to Okta. Auth0 is a known quantity. We wanted to get away from Auth0, to the more general, less
unneeded stuff, and less whimsical company, which Cognito promised.

So if we now go back, there seems to be no reason to go to Okta over Auth0.

But how difficult is it to create an STS in lambda?

## Lambda STS

https://developer.okta.com/blog/2018/11/13/create-and-verify-jwts-with-node (ironically, a page by Okta) shows how
simple it is to create a JWT token.

- We need a `token` endpoint to do that, with our custom logic.
- Then we need a `.well-known` endpoint where we return the public certificate.
- We need a public / private certificate
- we need to deploy this with an API Gateway; do this with serverless

And the general things, we need in all examples, to store and manage user info.

The main complexity is authenticating to the STS, to identify the user.

The client normally sends the refresh token to get an access token. But that is opaque. So, we can choose:

- either send the Cognito id or access token, to know the `sub`
- send the refresh token, and have the sts get an access token with that

The latter is then very similar to the OAuth2 application flow, so it might be interesting to do just that. The client
then would connect to the STS via older methods, such as session cookies, but that means it becomes stateful.

So, let's not do that.

The most straightforward path now is to use the refresh token. That introduces more latency.

`njwt` does offer asymetric keys: https://github.com/jwtk/njwt/issues/34 (although the README doesn't explain that).

`jsonwebtoken` is known however, and seems far better.

https://nodejs.org/api/crypto.html#crypto_crypto_generatekeypair_type_options_callback could be used to generate the key
pairs. Nope, that is only available since 10.12.0.

We need an RSA keypair. We should create the keys ourselves, and store the private key in AWS KMS.

https://github.com/jonathankeebler/jwt-kms seems to do a lot and https://www.npmjs.com/package/kms-jwt too.
