const Boom = require('boom')
const getAccessToken = require('./getAccessToken')
const Hoek = require('hoek')
const uuid = require('uuid')

const configuration = {}
const accessTokenGetter = getAccessToken.bind(configuration)

module.exports.handler = async function handler (event, context) {
  const sot = new Date()
  try {
    const request = Hoek.clone(event.request)
    if (!request.headers['x-flow-id']) {
      request.headers['x-flow-id'] = uuid.v4()
    }
    if (request.path === '/token') {
      return await accessTokenGetter(sot, request)
    } else if (request.path === '/.well-known/certificates') {
    } else {
      throw Boom.notFound(' no such endpoint')
    }
  } catch (err) {
    if (err.isBoom) {
      return err.output
    }
    return new Boom(err).output // 500
  }
}
