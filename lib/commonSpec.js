/**
 * @typedef {object} Joi
 * @property {function} alternatives
 * @property {function} any
 * @property {function} array
 * @property {function} date
 * @property {function} func
 * @property {function} number
 * @property {function} object
 * @property {function} ref
 * @property {function} string
 * @property {function} valid
 */

const /** @type {Joi} */ Joi = require('joi')

const flowId = Joi.string()
  .uuid('uuid/v4')
  .required()
  .description(
    'ID of the request, over all intermediate infrastructure. Normally created by the client, but since this is not part of the spec, the server adds it when it is missing.'
  )

/**
 * @typedef {object} MinimalHeader
 * @property {string} content-type - token requests are always www-form-urlencoded
 * @property {string} accept - we only support application/json, so this must appear in the Accept header
 * @property {string} x-flow-id
 */
const minimalHeader = Joi.object({
  'content-type': Joi.string()
    /* unsure what API Gateway / Lambda does, and irrelevant
                                                       .valid('application/x-www-form-urlencoded')
                                                       */ .description(
      'Token requests are always www-form-urlencoded'
    )
    .required(),
  accept: Joi.string()
    .regex(/application\/json/)
    .description('We only support application/json, so this must appear in the Accept header')
    .required(),
  'x-flow-id': flowId
})
  .unknown(true)
  .pattern(/[a-z0-9-]+/, Joi.string())
  .required()
  .description('The headers of the original token request. Header names are lower case.')

/**
 * @typedef BasicRequest
 * @property {MinimalHeader} headers
 * @property {object} body
 */
const basicRequest = Joi.object({
  headers: minimalHeader,
  body: Joi.object()
    .unknown(true)
    .required()
    .description('The body of the original request as JavaScript object')
}).label('request')

/**
 * @typedef {Logger} RequestLogger
 * @property {function} start
 * @property {function} result
 * @property {function} note
 */
const requestLogger = Joi.object()
  .keys({ start: Joi.func().required(), result: Joi.func().required(), note: Joi.func().required() })
  .unknown(true)
  .required()

const sot = Joi.date()

module.exports = { flowId, sot, minimalHeader, requestLogger, basicRequest }
