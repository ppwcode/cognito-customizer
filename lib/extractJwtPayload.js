const Contract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const validateSchema = require('./_util/validateSchema')
const jwtSpec = require('./jwtSpec')
const verify = require('util').promisify(require('jsonwebtoken').verify)
const depromisify = require('./_util/depromisify')
const date2SecSinceEpoch = require('./_util/date2SecSinceEpoch')
const { getSigningKey } = require('./userPool/signingKeyGetter')
const flowLogger = require('./_util/flowLogger').schema

/**
 * Verify the `token`, and extract the payload. Any failure is a configuration error, since we expect the provided
 * `token` to be fresh from a User Pool. It should not be expired, from an unexpected issuer or for an unexpected
 * audience, or for wrong scopes, since the User Pool gladly returned it. Hence, all errors are unexpected.
 *
 * @param {FlowLogger} logger
 * @param {Date} sot - Start of transaction. Used to validate the `token` "exp" against.
 * @param {JwtBearerToken} token - The JWT Bearer Access Token to verify and unpack
 * @returns {JwtPayload} The payload of `token`, if it verifies.
 * @property {Contract} contract
 */
const extractJwtPayload = new Contract({
  pre: [
    logger => validateSchema(flowLogger, logger),
    (logger, userPoolSigningKeyGetter) => getSigningKey.isImplementedBy(userPoolSigningKeyGetter),
    (logger, userPoolSigningKeyGetter, sot) => sot instanceof Date,
    (logger, userPoolSigningKeyGetter, sot, token) => token
  ],
  post: [(logger, userPoolSigningKeyGetter, sot, token, result) => validateSchema(jwtSpec.jwtPayload, result)]
}).implementation(async function extractJwtPayload (logger, userPoolSigningKeyGetter, sot, token) {
  logger = logger.start('extractJwtPayload', { sot, token })
  const keyGetter = depromisify(header => userPoolSigningKeyGetter(header.kid))

  const result = await verify(token, keyGetter, {
    algorithms: [jwtSpec.algorithm],
    ignoreNotBefore: true, // sot might be earlier; AWS User Pool does not include nbf anyway
    complete: false,
    clockTimestamp: date2SecSinceEpoch(sot)
  })
  return logger.nominal(result)
})

module.exports = extractJwtPayload
