const Contract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const validateSchema = require('./_util/validateSchema')
const rsaSpec = require('./rsaSpec')
const jwtSpec = require('./jwtSpec')
const Joi = require('joi')
const sign = require('util').promisify(require('jsonwebtoken').sign)
const flowLogger = require('./_util/flowLogger').schema

/**
 * @typedef {object} SignResult
 * @property {JwtHeader} header
 * @property {JwtBearerToken} token - Bearer access token, signed with the private key identified by `header.kid`
 */
const resultSchema = Joi.object({
  header: jwtSpec.jwtHeader.required(),
  token: jwtSpec.jwtBearerToken.required()
})
  .required()
  .label('signJwt result')

/**
 * Construct a Bearer Access Token from `payload`. Necessary claims are added to a clone of the provided
 * `payload` object. It cannot have properties `token_use`, nor `exp`. These are added by the algorithm.
 *
 * The token is always signed with the RS256 algorithm.
 *
 * No exceptions are possible when the preconditions are respected.
 *
 * // TODO express more stringent the preconditions on the keypair.privateKey, in the schema
 *
 * @param {FlowLogger} logger
 * @param {JwtPayload} payload - Base payload of the token. Mandatory.
 * @param {Object} keypair - Key pair. We need the `id` and the `privateKey`.  Mandatory.
 * @param {string} keypair.kid - Id of the key pair. Mandatory.
 * @param {string} keypair.privateKey - Private key of a key pair, in PEM PCKS1 format. Mandatory.
 * @returns {SignResult} Bearer access token, signed with `privateKey.key`, with the `privateKey.kid` mentioned in the
 *                       token header.
 * @property {Contract} contract
 */
const signJwt = new Contract({
  pre: [
    logger => validateSchema(flowLogger, logger),
    (logger, payload) => validateSchema(jwtSpec.jwtPayloadWithMandatoryNbf, payload),
    (logger, payload, keyPair) => validateSchema(rsaSpec.keypair, keyPair)
  ],
  post: [
    (logger, payload, keypair, result) => validateSchema(resultSchema, result),
    (logger, payload, keypair, result) => result.header.kid === keypair.id
  ]
}).implementation(async function signJwt (logger, payload, keypair) {
  logger = logger.start('signJwt', { payload, keypair })
  // noinspection JSUnusedGlobalSymbols
  const token = await sign(payload, keypair.privateKey, {
    algorithm: jwtSpec.algorithm,
    keyid: keypair.id
  })
  return logger.nominal({
    header: {
      alg: jwtSpec.algorithm,
      kid: keypair.id,
      typ: 'JWT'
    },
    token
  })
})

module.exports = signJwt
