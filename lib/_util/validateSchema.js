/**
 * @typedef {function} ContractFunction
 * @property {Contract} contract
 * @property {function} implementation
 * @property {object} [context]
 */

function validateSchema (schema, value, context) {
  const error = schema.validate(value, { convert: false, context }).error
  if (!error) {
    return true
  }
  console.error(error.annotate())
  return false
}

module.exports = validateSchema
