const os = require('os')
const pack = require('../../package') // MUDO should be package.json of the app using this library
const Joi = require('joi')
const bunyan = require('bunyan')
const Hoek = require('hoek')

/**
 * Mask the values of properties that have a name that is often used for sensitive data, recursively.
 *
 * Recursive function, to be called without `key` or `visited`. When called with an object as `value`, and without a
 * `key`, the property values of `value` might be changed, and `value` is returned. When called with an object as
 * `value` with a `key` that is not recognised as sensitive property name, a new object is returned that has the same
 * properties, with recursively masked property values.
 *
 * @param {*} value - the object (or other value) to mask properties of.
 * @param {string|number} [key] - The key `value` is the value of in an enclosing object or array. If `undefined``,
 *                                `value` is considered the root, and will be returned.
 * @param {Map} [visited] - Circular structure protection
 * @returns {*} `undefined` if `value` is a function, or `key` is 'logger'. '*****' if `key` indicates sensitive data.
 *              `value` if it is an object, and there is no `key`, or `value` is a Date or a primitive value. A new
 *              object with masked property values if `value` is an object, and there is a `key`. A new array with
 *              masked elements if `value` is an array.
 */
function masked (value, key, visited) {
  if (typeof value === 'function' || key === 'logger') {
    return undefined
  }
  if (value instanceof Date || (visited && visited.has(value))) {
    return visited.get(value)
  }
  if (key && (/.*(authorization|password|secret|token|code_verifier|private).*/i.test(key) || key === 'code')) {
    return '*****'
  }
  const sureVisited = visited || new Map()
  if (Array.isArray(value)) {
    const result = []
    sureVisited.set(value, result)
    value.forEach((v, index) => {
      result[index] = masked(v, index, sureVisited)
    })
    return result
  }
  if (value && typeof value === 'object') {
    /* The root info object CANNOT BE CHANGED (or winston does not work).
         The nested objects MUST BE CLONED (we do not want to change real objects, like the request, with logging). */
    const result = key === undefined ? value : {}
    sureVisited.set(value, result)
    Object.keys(value).forEach(k => {
      result[k] = masked(value[k], k, sureVisited)
    })
    return result
  }
  return value
}

const requiredFunction = Joi.func().required()

const flowFields = Joi.object({
  name: Joi.string().required(),
  application: Joi.object({ name: Joi.string().required(), version: Joi.string().required() }).required(),
  hostname: Joi.string()
    .hostname()
    .required(),
  pid: Joi.number()
    .integer()
    .min(0)
    .required(),
  flowId: Joi.string()
    .uuid({ version: 'uuidv4' })
    .required(),
  sot: Joi.date().required(),
  sequence: Joi.array().items(Joi.alternatives().try(Joi.number(), Joi.string()))
})
  .unknown(true)
  .required()

const functionFields = flowFields.keys({ function: Joi.string().required() })

/**
 * Bunyan logger, with extra methods to help with logging the flow through business functions. The Bunyan logger
 * is setup to output JSON to `stdout`, to mask all sensitive data, and to include default information in each log
 * message.
 *
 * The log levels are `fatal`, `error`, `warn`, `info`, `debug`, and `trace`. `FlowLogger` is to be used during the
 * handling of one request. An `error` ends the request, but not the program (which is the definition of `fatal`).
 *
 * At the start of a flow, create a new logger with `create`, and pass it to the first business function.
 *
 * Mark the start of each business function with a call to `start`, and remember the returned logger. That logger should
 * be used to mark all ends of the business function, with a call to `nominal` or `exceptional`, or `unexpected`.
 * `nominal` and `exceptional` endings are defined endings, and thus expected behavior. They are logged at `info` level.
 * All undefined endings, e.g., as a result of unexpected behavior, programming errors, are violations of internal or
 * external, explicit or implicit preconditions. They are logged at `error` level. They should end the request
 * (`error`), and not the program (`fatal`).
 *
 * `exceptional` and `unexpected` endings should only be logged if they have explicit code paths in the function. _No
 * code paths should be added merely to log non-nominal endings._ In fact, the quality of the code is higher if there
 * are no code paths for non-nominal endings. For one, they are more difficult to test.
 *
 * In the body of the method, more detail can be logged using `warn`, `info`, `verbose`, `debug`, or `silly`, and extra
 * information can be gathered in the logger by creating a new `child` logger, replacing the existing logger. `warn`
 * should be used to mark expected behavior, of which administrators should become aware. `info` level logs mark the
 * expected behavior of the program, in a way that makes it possible to reconstruct the decisions made by the program
 * later on. `debug` and `silly` should be used to output extra logs in the expected flow, which is not necessary to
 * reconstruct the decisions made by the program. `debug` and `silly` logs are usually added during development, and
 * should be kept. Use the `silly` level inside loops and recursive functions.
 *
 * When a business method is called from the current business method, the logger should be passed as argument. The
 * secondary business method should also follow this reasoning.
 *
 * The logger can also be passed to technical or helper functions that are used by the business function. Technical and
 * helper functions should only log at `debug` or `silly` level, if they log at all.
 *
 * // MUDO describe extra information
 *
 * @interface FlowLogger
 * @property {string} functionName - name of the function this logger is created for with `start`
 * @property {Array<number|string>} sequence - UML communication diagram inspired sequence. The last number is increased
 *                                             with each consecutive logging call. `create` starts sequence with a first
 *                                             number, that reflects a count of created loggers. A child logger created
 *                                             with `start` adds a number. Concurrent execution should be marked by
 *                                             calling `concurrent` with a `name`. This returns a child logger the
 *                                             concurrent flow. Counting starts at with `1`.
 */
const flowLogger = Joi.object({
  fields: flowFields,
  start: requiredFunction
})
  .unknown()
  .type(bunyan)
  .label('logger')
  .required()

const functionLogger = flowLogger.keys({
  fields: functionFields,
  concurrent: requiredFunction,
  nextSequence: requiredFunction,
  nominal: requiredFunction,
  exceptional: requiredFunction,
  unexpected: requiredFunction
})

function decorateLogger (flowLogger) {
  flowLogger.start = start
}

function decorateFunctionLogger (functionLogger) {
  decorateLogger(functionLogger)
  functionLogger.note = note
  functionLogger.concurrent = concurrent
  functionLogger.nextSequence = nextSequence
  functionLogger.nominal = nominal
  functionLogger.exceptional = exceptional
  functionLogger.unexpected = unexpected
}

/**
 * Returns a `child` logger of `this` and logs the start of the function with the given `functionName` with the given
 * `args` arguments at `info` level. The `functionName` is remembered. `sequence` gets an extra number `1`.
 *
 * @example
 * function <BUSINESS FUNCTION NAME> (logger, ARG1, ARG2, …) {
 *   logger = logger.start('<BUSINESS FUNCTION NAME>', {ARG1, ARG2, …})
 *   …
 * }
 *
 * @name FlowLogger#start
 * @param {string} functionName - name of the function this logs the start of; remembered
 * @param {object} args - Object with the formal parameters of the function `functionName` as keys and the actual
 *   parameters
 * @returns {FlowLogger}
 */
function start (functionName, args) {
  const sequence = this.fields.sequence.slice()
  sequence.push(1)
  const functionLogger = this.child({ function: functionName, sequence })
  decorateFunctionLogger(functionLogger)
  functionLogger.info({ args: Hoek.clone(args) }, `${functionName} called`)
  return functionLogger
}

/**
 * Returns a `child` logger of `this` with `name` added to `sequence`. Must be followed by `start`.
 *
 * @example
 *   …
 *   logger = logger.note({… <MORE FIELDS> …}
 *   …
 *
 * @name FlowLogger#note
 * @param {object} moreFields - object carrying extra fields to show in log statements
 * @returns {FlowLogger}
 */
function note (moreFields) {
  const functionLogger = this.child(moreFields)
  decorateFunctionLogger(functionLogger)
  return functionLogger
}

/**
 * Returns a `child` logger of `this` with `name` added to `sequence`. Must be followed by `start`.
 *
 * @example
 *   …
 *   const allResults = await Promise.all([
 *     <FLOW A ASYNC FUNCTION NAME>(…, logger.concurrent('<FLOW A>'), …)
 *   ])
 *   …
 *
 * @name FlowLogger#concurrent
 * @param {string} name - name of concurrent flow
 * @returns {FlowLogger}
 */
function concurrent (name) {
  const sequence = this.fields.sequence.slice()
  sequence.push(name)
  const functionLogger = this.child({ sequence }, true)
  decorateFunctionLogger(functionLogger)
  return functionLogger
}

function nextSequence () {
  this.fields.sequence[this.fields.sequence.length - 1]++
}

/**
 * Mark the nominal end of the function. `info` logs the `result`. The last number of sequence is increased.
 *
 * @example
 *   …
 *   return logger.nominal(<BUSINESS FUNCTION RESULT>)
 * }
 *
 * @name FlowLogger#nominal
 * @param {*} result - the result of the function to be reported
 * @returns {*} The `result` passed in.
 */
function nominal (result) {
  this.nextSequence()
  this.info({ result: Hoek.clone(result) }, `${this.fields.function} ends nominally`)
  return result
}

/**
 * A Boom error, and thus have an `output` property (with `statusCode`, `headers` and `payload`).
 *
 * @typedef {Boom} HttpException
 * @property {object} output - Standard Boom `output` object, holding the HTTP response information
 * @property {number} output.statusCode - Status code of the HTTP response.
 * @property {object} output.headers - Headers of the HTTP response. All keys should be lower case, and values
 *                                               should be strings.
 * @property {object} output.payload - Payload of the HTTP response. Must be stringifiable.
 */

/**
 * Mark the exceptional (but defined) end of the function. `info` logs the `exception.output`, if it exists, and the
 * `exception` otherwise. The last number of sequence is increased.
 *
 * @example
 *   …
 *   throw logger.exceptional(<BUSINESS FUNCTION EXCEPTION>)
 *   …
 * }
 *
 * @name FlowLogger#exceptional
 * @param {HttpException} exception - The exception should be a Boom error, and thus have an `output` property (with
 *                                   `statusCode`, `headers` and `payload`).
 * @returns {HttpException} The `exception` passed in.
 */
function exceptional (exception) {
  this.nextSequence()
  // for Boom, only report err.output
  this.info(
    {
      exception: exception && exception.output ? Hoek.clone(exception.output) : Hoek.clone(exception)
    },
    `${this.fields.function} ends exceptionally`
  )
  return exception
}

/**
 * Mark the unexpected (undefined) end of the function. `error` logs the `error`. The last number of sequence is
 * increased.
 *
 * @example
 *   …
 *   throw logger.unexpected(<BUSINESS FUNCTION ERROR>)
 *   …
 * }
 *
 * @name FlowLogger#unexpected
 * @param {Error} err
 * @returns {Error} The `err` passed in.
 */
function unexpected (err) {
  this.nextSequence()
  this.error(
    {
      err
    },
    `${this.fields.function} ends unexpected - the flow should be stopped asap`
  )
  return err
}

/**
 * Count the number of times `create` has been called
 * @type {number}
 */
let counter = 0

// MUDO must imply start; there is no reason not to
/**
 * Create a  Winston `FlowLogger`. `info` log default information about the process and the flow. `sequence` is
 * the count of FlowLoggers created with this method. Must be followed by `start`.
 *
 * @param {Date} sot - Start of Transaction
 * @param {string} flowId - uuid identifying the flow
 * @param {string} [level=debug] - Maximum level to log during the request. Should be `silly` in tests, and `info` in
 *   production.
 * @returns {FlowLogger & Logger}
 */
function create (sot, flowId, level = 'info') {
  counter++
  const logger = bunyan.createLogger(
    Object.assign(
      {
        name: `flow logger ${flowId}`,
        // `level` cannot be used together with `streams`
        streams: [
          {
            name: 'stdout',
            stream: process.stdout,
            level: level
          }
        ],
        serializers: {
          err: bunyan.stdSerializers.err
        },
        src: false
      },
      {
        application: { name: pack.name, version: pack.version },
        hostname: os.hostname(),
        pid: process.pid,
        flowId,
        sot,
        sequence: [counter]
      }
    )
  )
  decorateLogger(logger)
  logger.info(
    {
      process: { node: process.version, uptime: process.uptime() }
    },
    'logger created'
  )

  return logger
}

create.schema = flowLogger
create.functionLogger = functionLogger

/* Exposed for testing purposes. Consider private. */
create._ = {
  masked
}

module.exports = create
