const Contract = require('@toryt/contracts-iv/lib/IV/Contract')

/**
 * `date`, expressed as seconds since Epoch. Fractions of seconds are discarded (floor).
 *
 * @param {Date} date
 * @returns {Number}
 */
const date2SecSinceEpoch = new Contract({
  pre: [date => date instanceof Date],
  post: [
    (date, result) => Number.isInteger(result),
    (date, result) => result >= 0,
    (date, result) => result * 1000 <= date.getTime(),
    (date, result) => date.getTime() <= (result + 1) * 1000
  ]
}).implementation(function (date) {
  return Math.floor(date.getTime() / 1000)
})

module.exports = date2SecSinceEpoch
