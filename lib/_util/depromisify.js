function depromisify (asyncFunction) {
  return function (...args) {
    const callback = args.pop()
    return asyncFunction.apply(this, args).then(result => callback(null, result), error => callback(error))
  }
}

module.exports = depromisify
