const Contract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const jwtSpec = require('./jwtSpec')
const Boom = require('boom')
const uuid = require('uuid')
const signJwt = require('./signJwt')
const validateSchema = require('./_util/validateSchema')
const date2SecSinceEpoch = require('./_util/date2SecSinceEpoch')
const decoratePayload = require('./decoratePayload')
const payloadDecoratorContract = require('./payloadDecoratorContract')
const Joi = require('joi')
const getUserPoolTokens = require('./userPool/getTokens')
const extractJwtPayload = require('./extractJwtPayload')
const userPoolConditions = require('./userPool/common')
const userPoolSigningKeyGetter = require('./userPool/signingKeyGetter')
const commonSpec = require('./commonSpec')
const flowLogger = require('./_util/flowLogger').schema

/* MUDO: Lambda only allows JSON bodies
         API Gateway will convert application/x-www-form-urlencoded into JSON, possibly using
         https://claygardner.io/blog/api-gateway-any-content-type/ seems to be the solution
         https://forums.aws.amazon.com/thread.jspa?messageID=673012&tstart=0#673012 seems to be earlier
         https://blog.summercat.com/using-aws-lambda-and-api-gateway-as-html-form-endpoint.html is the context (just pass the string) */

/**
 * @typedef {object} GetAccessTokenConfiguration
 * @property {function} payloadDecorator - Custom implementation of `decoratePayload.decorator`
 * @property {string} issuer - Identifies (in the form of a URI) the principal that issued this JWT access token,
 *                             i.e., this program instance.
 * @property {number} defaultExpirationSeconds - The number of seconds a returned JWT Bearer Access Token can be used
 *                                               after `sot` by default. Can be overwritten per issued token by
 *                                               `payloadDecorator`.
 * @property {function} userPoolSigningKeyGetter - Function to get the signing key for a given "kid" from the User Pool
 *                                                 behind this STS. Create with `userSigningPoolGetter`.
 */
const getAccessTokenConfiguration = userPoolConditions.configurationSchema
  .keys({
    payloadDecorator: Joi.func() // Joi idiosyncrasy keeps us from specifying contract properties and min args
      .required()
      .description('Custom implementation of `decoratePayload.decorator`'),
    issuer: jwtSpec.issuer,
    defaultExpirationSeconds: Joi.number()
      .integer()
      .min(1)
      .required()
      .label('configuration')
      .description(
        `The number of seconds a returned JWT Bearer Access Token can be used after the Start of Transaction by default. Can 
be overwritten per issued token by \`payloadDecorator\`.`
      )
      .example(300),
    userPoolSigningKeyGetter: Joi.func() // Joi idiosyncrasy keeps us from specifying contract properties and min args
      .required()
      .description('Function to get the signing key for a given "kid" from the User Pool behind this STS.')
  })
  .unknown(true)
  .label('configuration')
  .required()

/**
 * @param {function} payloadDecorator - Custom implementation of `decoratePayload.decorator`
 * @param {Date} sot Start of transaction. Used, a/o, as `iat` in the returned access token
 * @param {BasicRequest} request - The request object that represents the HTTP `POST` request that the client sent
 *                                   for the token being generated.
 * @returns {TokenRequestResponsePayload}
 */
const getAccessToken = new Contract({
  pre: [
    configuration => validateSchema(getAccessTokenConfiguration, configuration),
    configuration => payloadDecoratorContract.isImplementedBy(configuration.payloadDecorator),
    configuration => userPoolSigningKeyGetter.getSigningKey.isImplementedBy(configuration.userPoolSigningKeyGetter),
    (configuration, logger) => validateSchema(flowLogger, logger),
    (configuration, logger, sot) => sot instanceof Date,
    (configuration, logger, sot, request) => validateSchema(commonSpec.basicRequest, request)
  ],
  post: [
    // only ends nominally when the request was an accessTokenRequest according to the standard
    (configuration, logger, sot, request) => validateSchema(jwtSpec.accessTokenRequest, request),
    (configuration, logger, sot, request, result) =>
      validateSchema(jwtSpec.tokenRequestResponsePayload, result, request)
  ],
  exception: [
    (configuration, logger, sot, request, exc) => Boom.isBoom(exc),
    (configuration, logger, sot, request, exc) => validateSchema(jwtSpec.tokenRequestErrorOutput, exc.output)
  ]
}).implementation(async function getAccessToken (configuration, logger, sot, request) {
  logger = logger.start('getAccessToken', { configuration, request })
  const userPoolTokens = /* @type TokenRequestResponsePayload */ await getUserPoolTokens(configuration, logger, request)
  logger.debug('User pool tokens acquired. User is authenticated and request is a valid OAuth2 token request.')
  const refreshToken = userPoolTokens.refresh_token || request.body.refresh_token
  const userPoolAccessTokenPayload = await extractJwtPayload(
    logger,
    configuration.userPoolSigningKeyGetter,
    sot,
    userPoolTokens.access_token
  )
  logger = logger.note({
    authn: {
      clientId: userPoolAccessTokenPayload.client_id,
      sub: userPoolAccessTokenPayload.sub,
      aud: userPoolAccessTokenPayload.aud,
      scope: userPoolAccessTokenPayload.scope
    }
  })
  // noinspection SpellCheckingInspection
  const keypair = {
    id: '708c8e8f-67c5-44f3-b98a-26537dfd0d5a',
    publicKey:
      '-----BEGIN RSA PUBLIC KEY-----\nMIIBCgKCAQEAjuaPI4gWnkbQH3JKNtTSfkdP36FZuOVC6SQITuNb4nUi0fb2BKn4Ik6LJikz\nxhvh3EjbksgXnmv1LEA1y5vZE4z8/OVJLkw+oHGrvhRhzNiCNni+a6ufIcBlyO+ETH3j5bK2\n+5St4l9ett7amW7jKfjvC7hxmPBlN/DMSgdSJS3/+U71UMZbxL3SHvYoSewe8ykdlfLph43X\nnCe3Le+Ih/CuIMDI5c4RD2bCFDQYtwJZGyRUC0K7HtsnWYjEpHLPoh7DEUM4C2w8nHLxiMno\nG2rfYsC64c7M1LDbUP0BUgaZfkOffS+s8Qfnhd33QlVANGdUyQxjqMgimS8RCcnTBQIDAQAB\n-----END RSA PUBLIC KEY-----\n',
    privateKey:
      '-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAjuaPI4gWnkbQH3JKNtTSfkdP36FZuOVC6SQITuNb4nUi0fb2BKn4Ik6L\nJikzxhvh3EjbksgXnmv1LEA1y5vZE4z8/OVJLkw+oHGrvhRhzNiCNni+a6ufIcBlyO+ETH3j\n5bK2+5St4l9ett7amW7jKfjvC7hxmPBlN/DMSgdSJS3/+U71UMZbxL3SHvYoSewe8ykdlfLp\nh43XnCe3Le+Ih/CuIMDI5c4RD2bCFDQYtwJZGyRUC0K7HtsnWYjEpHLPoh7DEUM4C2w8nHLx\niMnoG2rfYsC64c7M1LDbUP0BUgaZfkOffS+s8Qfnhd33QlVANGdUyQxjqMgimS8RCcnTBQID\nAQABAoIBAQCOGbNpL+DeD6jSPIKUN9oBfMRuqzJfbIu27v/cArbSYIz5oc1PIf3/j39LuVkk\nvYFB3qmKMrNZ9BzfnhJgoF+i02aXzSGSinsUbTTNVdNTMlF5/WPOCeG6XGpa/+Lddap0Nd1E\nG7s2CoRS8RUL0nrOuB5t10IPRa5BjJB5ZQJIuRLJt5T82693od85TZuUg2TKACBYUfFOVqLN\nfNDDBI6coMoaIiF8fEJx0mCudLSLWi6MT7DLS7pWbK6CtsPAhAuydYQ9N0/F3kfOaT2o+QEx\nP39TRLp5pN6z95DRGsSanJ39l5FvvDH6XvO1dyotKDLDtvOgEuxNnsEWUyko99lZAoGBAMcL\n5zIBG0TDvvX/2JVxXOgiYtOWS3paw4/BHB8weZBgjXcnj2+eYS3IUJQyYGPVJk15t+lbPJIa\ndm2HNRzESOfgyXVemRvtBQ8mAEbB9pc73Y3k81YbcF9QgAz4VZ4KrNd6NskbWllTfpw5s8Eh\n8L4MnPPfUlnEd6VH4d2dP5gPAoGBALfJ/MGez5DT2PkZXobTQKcyDHH0VPWivvqOaGhvo+Qt\npMHy9jUbF8KkWLB3sJOoTQdCrrpi+34h7e8NCE+wzma4WT6iZUzAwTyLUXU6DWO8RC5azUzl\n3BrZKgXdW4cdNEfVKDyGkDUVUZ2db4eWiQp2wY72ic6O8WChNTGd06+rAoGAFqIH2+u8Sglo\nAVjiK7wEMHEYg66nTnZbnlD6/aDpcb1I2K+q46pCqo4Ie5Fu3Gs0O8MHYoV5UbOom7OwGmFH\nWPZ6cdob1s0QsjRD+8e6Xl/0RVovQS9Fi9D/fnoOYjYciTPgXuW5VEbmMqVtBxzw3utYPXK7\nTnHEmQqfaC+lN4kCgYBwfQBmwXzpd8BlNlHU05fOvaNAW/tkHKgnUuI0iaAWGJmu27Y16s8w\nIsblu1woA+qhdv7atZqYKMwodCGSJPUvicWVwG+f+ppLJDpHNbDcSm6wsILXfhipliFSVZmf\nqRBGpwBpizLYSqQZhDKvGkPmU6lEh0DxXCwAqKqWI7Nc4QKBgBcl/X6x63dqAnGv73/v/THC\n8wOg85UC5+ZxkJU4HwDcAQlD4rV80q0kwEcySjL+bGf8ntidD3S/YfcNz8nfM+PZVZFwZ4jP\nMUi4pznVBjsG3nQBGcRbpOJpAC3WalbFPivRl3Q1CVYoHSBtpJ3Bi8yk9VmF64zqVA0+gJ3F\nlATX\n-----END RSA PRIVATE KEY-----\n'
  } // MUDO await ensurePrivateKey()
  const iat = date2SecSinceEpoch(sot)
  /* MUDO do not let the decorator change the scope!
          According to the standard we can return more scopes than requested, but can we return less?
          If we add scopes here, the client thinks he 'owns' them, and can request them again on a next request.
          However, if we add scopes here that are not known by Cognito, that request will be refused by getUserPoolTokens?
          (Yes, if we add the scopes in the client credentials call? Without, we get all the scopes. We cannot add
          scopes to the authorization code call (?), but we can with the refresh token call. But, the scopes requested
          by the client can never be more than the scopes requested in the original authorization code call! So, how to
          deal with this? Certain is that if getUserPool requests scopes that are unknown to Cognito, getUserPoolTokens
          will be refused! On the other hand, does it make sense for the decorator to change / add scopes?
          The decoration is for adding extra claims. Scopes cannot be used where extra claims (dynamic authorization)
          are needed. */
  const basePayload = {
    jti: uuid.v4(),
    token_use: 'access',
    iss: configuration.issuer,
    client_id: userPoolAccessTokenPayload.client_id,
    sub: userPoolAccessTokenPayload.sub,
    aud: userPoolAccessTokenPayload.aud,
    scope: userPoolAccessTokenPayload.scope,
    iat: iat,
    nbf: iat,
    exp: iat + configuration.defaultExpirationSeconds
  }
  const decoratedPayload = await decoratePayload(logger, configuration.payloadDecorator, request, basePayload) // might fail
  // TODO await rememberToken(decoratedPayload) and clean after a month?
  const signed = await signJwt(logger, decoratedPayload, keypair) // cannot fail
  return logger.nominal({
    access_token: signed.token,
    token_type: 'bearer',
    expires_in: decoratedPayload.exp - decoratedPayload.iat,
    refresh_token: refreshToken,
    scope: decoratedPayload.scope
  })
})

module.exports = getAccessToken
