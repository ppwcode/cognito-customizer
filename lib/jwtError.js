const Boom = require('boom')
const jwtSpec = require('./jwtSpec')
const Contract = require('@toryt/contracts-iv/lib/IV/Contract')
const validateSchema = require('./_util/validateSchema')

/**
 * @typedef JwtError
 * @property {boolean} isBoom
 * @property {TokenRequestErrorOutput} output - What will be returned as HTTP response when the error occurs
 */

/**
 * The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is invalid,
 * expired, revoked, does not match the redirection URI used in the authorization request, or was issued to another
 * client.
 *
 * @param {string} code - One of the standardized OAuth2 token request error codes
 * @param {string=} description - Human-readable ASCII text providing additional information, used to assist the client
 *                                developer in understanding the error that occurred.
 * @param {string=} uri - URI identifying a human-readable web page with information about the error, used to provide
 *                        the client developer with additional information about the error.
 * @return {JwtError}
 * @property {Contract} contract
 */
const jwtError = new Contract({
  pre: [
    code => validateSchema(jwtSpec.tokenRequestErrorCode, code),
    (code, description) => validateSchema(jwtSpec.tokenRequestErrorDescription, description),
    (code, description, uri) => validateSchema(jwtSpec.tokenRequestErrorUri, uri)
  ],
  post: [
    (code, description, uri, result) => Boom.isBoom(result),
    (code, description, uri, result) => result.message === `${code}: ${description}`,
    (code, description, uri, result) => result.data === null,
    (code, description, uri, result) => validateSchema(jwtSpec.tokenRequestErrorOutput, result.output),
    (code, description, uri, result) => result.output.payload.error === code,
    (code, description, uri, result) => result.output.payload.error_description === description,
    (code, description, uri, result) => result.output.payload.error_uri === uri
  ]
}).implementation(function jwtError (code, description, uri) {
  const statusCode = jwtSpec.tokenRequestErrorStatusCodes[code]
  const errorType = statusCode === 401 ? Boom.unauthorized : Boom.badRequest
  const err = errorType(`${code}: ${description}`)
  err.output.payload = {
    error: code,
    error_description: description,
    error_uri: uri
  }
  if (statusCode === 401) {
    err.output.headers[jwtSpec.wwwAuthenticateHeader] = jwtSpec.wwwAuthenticateContent
  }
  return err
})

Object.keys(jwtSpec.tokenRequestErrorStatusCodes).forEach(errorCode => {
  jwtError[errorCode] = jwtError.bind(null, errorCode)
})

module.exports = jwtError
