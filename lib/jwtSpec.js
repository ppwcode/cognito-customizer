const commonSpec = require('./commonSpec')
const /** @type {Joi} */ Joi = require('joi')

// Publication of https://tools.ietf.org/html/rfc7519, May 2015
const rfc7519PublicationSecSinceEpoch = Date.UTC(2015, 4, 1, 0, 0, 0, 0) / 1000
const algorithm = 'RS256'

const wwwAuthenticateHeader = 'www-authenticate'
const wwwAuthenticateContent = 'client_credentials method="POST"'

/* NOTE: Also see https://tools.ietf.org/id/draft-tschofenig-oauth-audience-00.html#rfc.section.3
   According to OAuth2 spec, should be an array in token.
   For now, we only work with 1 audience, a string */
const audience = Joi.string()
  .uri()
  /* Alternative. See NOTE
.array()
.items(Joi.string().uri())
.single()
.min(1)
.unique()
 */ .optional()
  .description(
    `Audience: identifies the recipients that the JWT access token is intended for. Each principal intended to process 
the JWT must identify itself with a value in the audience claim. If the principal processing the claim does not identify 
itself with a value in the "aud" claim, then the JWT MUST be rejected.`
  )
  .example('https://gateway.example.com/service1')
/* Alternative. See NOTE
.example([['https://gateway.example.com/service1', 'https://service2.ppwcode.org/'], {}])
*/

const clientId = Joi.string()
  .description('ID in the IdP of the Oauth2 Client this JWT access token was issued for')
  .example('089uy56h09jpoaz421')

const scope = Joi.string()
  .required()
  .description('The OAuth2 scope of the access token. Space separated list.')
  .example('grants:read data:sniff')

/* OAuth2 token request spec, + extra constraints from Cognito token endpoint.
   Notably, Authorization header is required when the client has a secret, which is always so (and in our case only)
   for client_credentials.
   https://docs.aws.amazon.com/cognito/latest/developerguide/token-endpoint.html

   https://tools.ietf.org/html/rfc6749#section-2.3.1 says to use Basic Auth */

/* NOTE: IMPORTANT These rules apply after we get the user pool tokens. Even then, some properties are irrelevant. */
const accessTokenRequestHeaders = commonSpec.minimalHeader.keys({
  /* authorization and content-type are irrelevant for handling after we get the user pool tokens */
  authorization: Joi.string()
    .regex(/^Basic [A-Za-z0-9=]+$/) // there are better patterns
    .optional()
    .description('Basic Authorization header') // required with client_credentials
})

/**
 * For use with the original authorization grant, to get the first access token and a refresh token.
 * We only support this flow with PKCS.
 *
 * @typedef {object} AuthorizationCodeAccessTokenRequestBody
 * @property {string} grant_type - 'authorization_code'
 * @property {string} code - The authorization code received from the authorization server.
 * @property {string} redirect_uri - The redirect URI used to acquire the authorization code passed in "code".
 * @property {string} client_id - ID in the IdP of the Oauth2 Client this JWT access token was issued for.
 */
const authorizationCodeAccessTokenRequestBody = Joi.object({
  grant_type: Joi.string()
    .equal('authorization_code')
    .required()
    .description('For use with the original authorization grant, to get the first access token and a refresh token.')
    .example('authorization_code'),
  /* code, code_verifier, redirect_url, and client_id are irrelevant for handling after we get the user pool tokens */
  code: Joi.string()
    .required()
    .description('The authorization code received from the authorization server')
    .example('jpoaz9023jpoaz93t/52n0t8u0562805'),
  code_verifier: Joi.string()
    .required()
    .description('The PKCS secret, used for this authorization code flow')
    .example('ho9lnf8923hu590h3852v302v89h5c49'),
  redirect_uri: Joi.string()
    .uri()
    .required()
    .description('The redirect URI used to acquire the authorization code passed in "code".')
    .example('https://example.com/loggedin.html'),
  client_id: clientId.required()
  // audience. scope makes no sense with authorization code
}).unknown(true)

/**
 * For use with the refresh token returned from an earlier call.
 *
 * @typedef {object} RefreshTokenAccessTokenRequestBody
 * @property {string} grant_type - 'refresh_token'
 * @property {string} refresh_token - The refresh token issued to the client in a previous interaction.
 * @property {string} scope - The scope of the access request. The requested scope must not include any scope not
 *                            originally granted by the resource owner, and if omitted is treated as equal to the scope
 *                            originally granted by the resource owner. Space separated.
 */
const refreshTokenAccessTokenRequestBody = Joi.object({
  grant_type: Joi.string()
    .equal('refresh_token')
    .required()
    .description('For use with the refresh token returned from an earlier call.')
    .example('refresh_token'),
  /* refresh_token and client_id are irrelevant for handling after we get the user pool tokens */
  refresh_token: Joi.string()
    .required()
    .description('The refresh token issued to the client')
    .example('l.jpoaz53.96a34u:p9/pu639'),
  client_id: clientId.required(), // required by Cognito
  audience: audience, // not "aud" in the request
  scope: scope.optional().description(`The scope of the access request. The requested scope must not include any scope 
not originally granted by the resource owner, and if omitted is treated as equal to the scope originally granted by the 
resource owner.`)
}).unknown(true)

/**
 * @typedef {object} ClientCredentialsAccessTokenRequestBody
 * @property {string} grant_type - 'client_credentials'
 * @property {string} client_id - The client identifier issued to the client during the registration process
 * @property {string} client_secret - The client secret issued to the client during the registration process
 * @property {string} scope - The scope of the access request. The requested scope must not include any scope not
 *                            originally granted by the resource owner, and if omitted is treated as equal to the scope
 *                            originally granted by the resource owner. Space separated.
 */
const clientCredentialsAccessTokenRequestBody = Joi.object({
  grant_type: Joi.string()
    .equal('client_credentials')
    .required()
    .description('For use with client_credentials.')
    .example('client_credentials'),
  audience: audience, // not "aud" in the request
  scope: scope.optional().description(
    `The scope of the access request. If no scope is provided, the access token gets all scopes defined for the 
client.`
  )
}).unknown(true)

/**
 * @typedef {object} AuthorizationCodeAccessTokenRequest
 * @property {object} headers
 * @property {AuthorizationCodeAccessTokenRequestBody} body
 */
const authorizationCodeAccessTokenRequest = Joi.object({
  headers: accessTokenRequestHeaders,
  body: authorizationCodeAccessTokenRequestBody
}).unknown(true)

/**
 * @typedef {object} RefreshTokenAccessTokenRequest
 * @property {object} headers
 * @property {RefreshTokenAccessTokenRequestBody} body
 */
const refreshTokenAccessTokenRequest = Joi.object({
  headers: accessTokenRequestHeaders,
  body: refreshTokenAccessTokenRequestBody
}).unknown(true)

/**
 * @typedef {object} ClientCredentialsAccessTokenRequest
 * @property {object} headers - The authorization header is required, but we need to return a 40(0|1),
 *                              so we do not make this a precondition
 * @property {ClientCredentialsAccessTokenRequestBody} body
 */
const clientCredentialsAccessTokenRequest = Joi.object({
  headers: accessTokenRequestHeaders.requiredKeys('authorization'),
  body: clientCredentialsAccessTokenRequestBody
}).unknown(true)

/**
 * @typedef {AuthorizationCodeAccessTokenRequest | RefreshTokenAccessTokenRequest | ClientCredentialsAccessTokenRequest} AccessTokenRequest
 */
const accessTokenRequest = Joi.alternatives()
  .try(authorizationCodeAccessTokenRequest, refreshTokenAccessTokenRequest, clientCredentialsAccessTokenRequest)
  .required()

/**
 * @typedef {object} JwtHeader
 * @property {string} kid - The id of the keypair used for signing the JWT.
 * @property {string} alg - The algorithm used for signing the JWT. This implementation only allows "RS256".
 * @property {string} type -The type of the token. This implementation only allows "JWT".
 */
const jwtHeader = Joi.object({
  kid: Joi.string()
    .required()
    .description('The id of the keypair used for signing the JWT.')
    .example('/Jpoaz8XyhJPOAZ+YUZJqmOJzW9X/rioMhfWaP1oqg='),
  alg: Joi.string()
    .equal('RS256')
    .required()
    .description('The algorithm used for signing the JWT. This implementation only allows "RS256".')
    .example('RS256'),
  typ: Joi.string()
    .equal('JWT')
    .required()
    .description('The type of the token. This implementation only allows "JWT".')
    .example('JWT')
})

/* NOTE: descriptions from
         https://tools.ietf.org/html/rfc7519 "JSON Web Token (JWT)"
         https://tools.ietf.org/html/rfc6750 "The OAuth 2.0 Authorization Framework: Bearer Token Usage" */
const issuer = Joi.string()
  .uri()
  .required()
  .description(
    `Issuer: identifies (in the form of a URI) the principal that issued this JWT access token, i.e., this program 
instance`
  )
  .example('https://ppwcode.org/example.com')

/**
 * @typedef {object} JwtPayloadBase
 * @property {string} jti - JWT ID: unique identifier for the JWT access token. The identifier value is assigned in a
 *                          manner that ensures that there is a negligible probability that the same value will be
 *                          accidentally assigned to a different data object; collisions are prevented among values
 *                          produced by different issuers instances. The "jti" claim can be used to prevent the JWT from
 *                          being replayed.
 * @property {string} sub - Subject: identifies the principal that is the subject of this JWT access token. The claims
 *                          in this JWT access token are statements about the subject. The subject value is scoped to be
 *                          locally unique in the context of the IdP.
 * @property {string} aud - Audience: identifies the recipients that the JWT access token is intended for. Each
 *                          principal intended to process the JWT must identify itself with a value in the audience
 *                          claim. If the principal processing the claim does not identify itself with a value in the
 *                          "aud" claim, then the JWT MUST be rejected.
 * @property {string} scope - The OAuth2 scope of the access token. Space separated list.
 * @property {string} iss - Issuer: identifies (in the form of a URI) the principal that issued this JWT access token,
 *                          i.e., this program instance.
 * @property {string} client_id - ID in the IdP of the Oauth2 Client this JWT access token was issued for.
 * @property {number} iat - Issued At: the time, in seconds since Epoch, at which the JWT was issued. This claim can be
 *                          used to determine the age of the JWT.
 */
const jwtPayloadBase = Joi.object({
  jti: Joi.string()
    .uuid({ version: 'uuidv4' })
    .required()
    .description(
      `JWT ID: unique identifier for the JWT access token. The identifier value is assigned in a manner that ensures 
that there is a negligible probability that the same value will be accidentally assigned to a different data object; 
collisions are prevented among values produced by different issuers instances. The "jti" claim can be used to prevent 
the JWT from being replayed.`
    )
    .example('6afa2b80-ae73-4279-ba77-a1b0d138f116'),
  sub: Joi.string()
    .required()
    .description(
      `Subject: identifies the principal that is the subject of this JWT access token. The claims in this JWT access 
token are statements about the subject. The subject value is scoped to be locally unique in the context of the IdP.`
    )
    .example('jpoaz7d3ele5hals9jpoaz3ek'),
  aud: audience,
  scope: scope.required(),
  iss: issuer,
  client_id: clientId.required(),
  iat: Joi.number()
    .integer()
    .min(rfc7519PublicationSecSinceEpoch)
    .required() // jsonwebtoken would like to use 'now' itself, but we won't let it
    .description(
      `Issued At: the time, in seconds since Epoch, at which the JWT was issued. This claim can be used to determine the
age of the JWT.`
    )
    .example(1554125079)
}).unknown(true)

/**
 * @typedef {object} JwtPayloadExtra
 * @property {string} token_use - 'The intended use of this JWT. This implementation only allows 'access'.
 * @property {number} exp - Expiry Date: the expiration time, in seconds since Epoch, on or after which the JWT must not
 *                          be accepted for processing. The processing of the "exp" claim requires that the current
 *                          date/time must be before the expiration date/time listed in the "exp" claim. Implementers
 *                          may provide for some small leeway, usually no more than a few minutes, to account for clock
 *                          skew.
 * @property {number} nbf - Not Before: the time before which the JWT must not be accepted for processing. The
 *                          processing of the "nbf" claim requires that the current date/time must be after or equal to
 *                          the not-before date/time listed in the "nbf" claim.  Implementers may provide for some small
 *                          leeway, usually no more than a few minutes, to account for clock skew.
 *
 * Minimal JWT Bearer Access Token payload, according to [RFC7519](https://tools.ietf.org/html/rfc7519) "JSON Web Token
 * (JWT)"
 * @typedef {JwtPayloadBase & JwtPayloadExtra} JwtPayload
 *
 */
const jwtPayload = jwtPayloadBase.keys({
  token_use: Joi.string()
    .equal('access')
    .required()
    .description('The intended use of this JWT. This implementation only allows "access"')
    .example('access'),
  exp: Joi.number()
    .integer()
    .greater(Joi.ref('iat'))
    .required()
    .description(`Expiry Date: the expiration time, in seconds since Epoch, on or after which the JWT must not be 
accepted for processing. The processing of the "exp" claim requires that the current date/time must be before the 
expiration date/time listed in the "exp" claim. Implementers may provide for some small leeway, usually no more than a 
few minutes, to account for clock skew.`),
  nbf: Joi.number()
    .integer()
    .min(Joi.ref('iat'))
    .less(Joi.ref('exp'))
    .optional()
    .description(`Not Before: the time before which the JWT must not be accepted for processing. The processing of the 
"nbf" claim requires that the current date/time must be after or equal to the not-before date/time listed in the "nbf" 
claim.  Implementers may provide for some small leeway, usually no more than a few minutes, to account for clock skew.`)
})

/**
 * @typedef {JwtPayload} JwtPayloadWithMandatoryNbf
 */
const jwtPayloadWithMandatoryNbf = jwtPayload.requiredKeys('aud', 'nbf')

/**
 * @typedef {JwtPayloadWithMandatoryNbf} StrictJwtPayload
 */
const strictJwtPayload = jwtPayloadWithMandatoryNbf
  .label('basePayload')
  .unknown(false)
  .required()

/**
 * @typedef {string} Token
 */
const token = Joi.string()
  // Joi.token() does not accept '.'
  .description(`A security token in general. In general this is an opaque string.`)

/**
 * @typedef {Token} JwtBearerToken
 */
const jwtBearerToken = token.description(`A security token with the property that any party in possession of the token 
(a "bearer") can use the token in any way that any other party in possession of it can.  Using a bearer token does not 
require a bearer to prove possession of cryptographic key material (proof-of-possession).`)

/**
 * @typedef {object} TokenRequestResponsePayload
 * @property {string} access_token - The requested JWT Bearer Access Token
 * @property {string} token_type - `bearer`
 * @property {number} expires_in - Difference (seconds) between the returned `access_token` `exp` and `iat`
 * @property {string} refresh_token - A new opaque refresh token, that can be used for future token requests
 * @property {string} scope - The scope mentioned in the `access_token`. Space separated.
 */
const tokenRequestResponsePayload = Joi.object({
  access_token: jwtBearerToken.required(),
  token_type: Joi.string()
    .equal('bearer')
    .insensitive()
    .required()
    .description('The type of the token issued. Defined in RFC6749. Overlaps with "type"')
    .example('bearer'),
  expires_in: Joi.number()
    .integer()
    .min(0)
    .required()
    .description('The lifetime in seconds of the access token.')
    .example(300),
  refresh_token: Joi.string()
    .when(Joi.ref('$body.grant_type'), {
      is: 'client_credentials',
      then: Joi.any().forbidden(),
      otherwise: Joi.any().required()
    })
    .description(
      `The refresh token, which can be used to obtain new access tokens using the same authorization grant. 
Not returned for a client credentials request.`
    )
    .example('fa5n2y853086b308iqy54b-0/jpoaz-90'),
  // MUDO id_token
  scope: scope.required()
    .description(`The scope of the access token handed out. This can be different from the requested scope. Optional in
the standard, but we make it required.`)
})

/* NOTE: https://tools.ietf.org/html/rfc6749#section-5.2 */
const tokenRequestErrorCode = Joi.string()
  .valid([
    /* The request is missing a required parameter, includes an unsupported parameter value (other than grant type),
       repeats a parameter, includes multiple credentials, utilizes more than one mechanism for authenticating the
       client, or is otherwise malformed. */
    'invalid_request',
    /* Client authentication failed (e.g., unknown client, no client authentication included, or unsupported
       authentication method).  The authorization server MAY return an HTTP 401 (Unauthorized) status code to indicate
       which HTTP authentication schemes are supported.  If the client attempted to authenticate via the "Authorization"
       request header field, the authorization server MUST respond with an HTTP 401 (Unauthorized) status code and
       include the "WWW-Authenticate" response header field matching the authentication scheme used by the client. */
    'invalid_client',
    /* The provided authorization grant (e.g., authorization code, resource owner credentials) or refresh token is
       invalid, expired, revoked, does not match the redirection URI used in the authorization request, or was issued to
       another client. */
    'invalid_grant',
    /* The authenticated client is not authorized to use this authorization grant type. */
    'unauthorized_client',
    /* The authorization grant type is not supported by the authorization server. */
    'unsupported_grant_type',
    /* The requested scope is invalid, unknown, malformed, or exceeds the scope granted by the resource owner. */
    // NOTE: not supported by Cognito: https://docs.aws.amazon.com/cognito/latest/developerguide/token-endpoint.html
    'invalid_scope'
  ])
  .required()
  .example('invalid_request')

const tokenRequestErrorStatusCodes = {
  invalid_request: 400,
  invalid_client: 401,
  invalid_grant: 400,
  unauthorized_client: 400,
  unsupported_grant_type: 400,
  invalid_scope: 400
}

const tokenRequestErrorDescription = Joi.string().optional()
  .description(`Human-readable ASCII text providing additional information, used to assist the client developer in 
understanding the error that occurred.`)

const tokenRequestErrorUri = Joi.string()
  .uri()
  .optional()
  .description(`URI identifying a human-readable web page with information about the error, used to provide the client 
developer with additional information about the error.`)

/**
 * @typedef {object} TokenRequestErrorPayload
 * @property {string} error - one of 'invalid_request', 'invalid_client', 'invalid_grant', 'unauthorized_client',
 *                            'unsupported_grant_type', 'invalid_scope'.
 * @property {string} [error_description] - Human-readable ASCII text providing additional information, used to assist
 *                                          the client developer in understanding the error that occurred.
 * @property {string} [error_uri] - URI identifying a human-readable web page with information about the error, used to
 *                                  provide the client developer with additional information about the error.
 */
const tokenRequestErrorPayload = Joi.object({
  error: tokenRequestErrorCode,
  error_description: tokenRequestErrorDescription,
  error_uri: tokenRequestErrorUri
})
  .unknown(false)
  .required()

/**
 * @typedef {object} TokenRequestErrorOutput
 * @property {number} statusCode - HTTP Status Code. Must be `400`, or `401` when `client_credentials` authentication
 *                                 failed.
 * @property {object} headers - HTTP response headers. Must contain 'www-authenticate' when `statusCode` is `401`, and
 *                              may not contain 'www-authenticate' when `statusCode` is `400`.
 * @property {TokenRequestErrorPayload} - OAuth2 standardized error response payload
 */
const tokenRequestErrorOutput = Joi.object({
  statusCode: Joi.number()
    .integer()
    .when('payload.error', {
      is: 'invalid_client',
      then: Joi.valid(401),
      otherwise: Joi.valid(400)
    })
    .required(),
  headers: Joi.alternatives()
    .when('payload.error', {
      is: 'invalid_client',
      then: Joi.object({
        [wwwAuthenticateHeader]: Joi.string()
          .valid(wwwAuthenticateContent)
          .required()
      }).unknown(true),
      otherwise: Joi.object({
        [wwwAuthenticateHeader]: Joi.any().forbidden()
      }).unknown(true)
    })
    .required(),
  payload: tokenRequestErrorPayload
}).required()

module.exports = {
  algorithm,
  wwwAuthenticateHeader,
  wwwAuthenticateContent,
  clientId,
  scope,
  audience,
  accessTokenRequestHeaders,
  authorizationCodeAccessTokenRequestBody,
  refreshTokenAccessTokenRequestBody,
  clientCredentialsAccessTokenRequestBody,
  authorizationCodeAccessTokenRequest,
  refreshTokenAccessTokenRequest,
  clientCredentialsAccessTokenRequest,
  accessTokenRequest,
  jwtHeader,
  issuer,
  jwtPayloadBase,
  jwtPayload,
  jwtPayloadWithMandatoryNbf,
  strictJwtPayload,
  token,
  jwtBearerToken,
  tokenRequestResponsePayload,
  tokenRequestErrorCode,
  tokenRequestErrorStatusCodes,
  tokenRequestErrorDescription,
  tokenRequestErrorUri,
  tokenRequestErrorPayload,
  tokenRequestErrorOutput
}
