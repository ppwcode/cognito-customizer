const Contract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const validateSchema = require('./_util/validateSchema')
const jwtSpec = require('./jwtSpec')
const Hoek = require('hoek')
const Boom = require('boom')
const payloadDecoratorContract = require('./payloadDecoratorContract')
const flowLogger = require('./_util/flowLogger').schema

/**
 * @param {Function} decorator - Async decorator function, that will be called with `request` and `basePayload`. Must
 *                               implement `decoratePayload.decorator`.
 * @param {AccessTokenRequest} request - The request object that represents the HTTP `POST` request that the client sent
 *                                       for the token being generated.
 * @param {JwtPayload} basePayload - The JWT Bearer Access Token Payload, that will be returned, containing the standard JWT
 *                               properties. Some can be overwritten by `decorator`, some cannot be overwritten.
 * @returns {JwtPayload} A new object, that is the merge of the `basePayload` and the result of the `decorator` call.
 * @throws {JwtError} `JwtError` with code 'invalid_request' or 'invalid_scope'
 * @property {Contract} contract
 */
const decoratePayload = new Contract({
  pre: [
    logger => validateSchema(flowLogger, logger),
    (logger, decorator) => Contract.isAContractFunction(decorator),
    (logger, decorator) => payloadDecoratorContract.isImplementedBy(decorator),
    (logger, decorator, request) => validateSchema(jwtSpec.accessTokenRequest, request),
    (logger, decorator, request, basePayload) => validateSchema(jwtSpec.strictJwtPayload, basePayload)
  ],
  post: [
    (logger, decorator, request, basePayload, result) => result !== basePayload,
    (logger, decorator, request, basePayload, result) => validateSchema(jwtSpec.jwtPayloadWithMandatoryNbf, result),
    (logger, decorator, request, basePayload, result) => result.jti === basePayload.jti,
    (logger, decorator, request, basePayload, result) => result.sub === basePayload.sub,
    (logger, decorator, request, basePayload, result) => result.aud === basePayload.aud,
    (logger, decorator, request, basePayload, result) => result.iss === basePayload.iss,
    (logger, decorator, request, basePayload, result) => result.client_id === basePayload.client_id,
    (logger, decorator, request, basePayload, result) => result.iat === basePayload.iat,
    (logger, decorator, request, basePayload, result) => result.token_use === basePayload.token_use
  ],
  exception: [
    (logger, decorator, request, basePayload, exception) => Boom.isBoom(exception),
    (logger, decorator, request, basePayload, exception) =>
      validateSchema(jwtSpec.tokenRequestErrorOutput, exception.output),
    (logger, decorator, request, basePayload, exception) =>
      payloadDecoratorContract.allowedErrorCodes.includes(exception.output.payload.error)
  ]
}).implementation(async function decoratePayload (logger, decorator, request, basePayload) {
  logger = logger.start('decoratePayload', { request, basePayload })
  const structuredRequest = Hoek.clone(request)
  delete structuredRequest.headers.authorization
  delete structuredRequest.headers['content-type']
  delete structuredRequest.headers.accept
  delete structuredRequest.body.client_id
  delete structuredRequest.body.aud
  delete structuredRequest.body.code
  delete structuredRequest.body.code_verifier
  delete structuredRequest.body.refresh_token
  delete structuredRequest.body.redirect_uri
  logger.info({ decoratorArgs: { request: structuredRequest, basePayload } }, 'calling decorator …')
  const decorations = await decorator(logger, structuredRequest, Hoek.clone(basePayload))
  logger.info({ decorations }, 'decorator returned nominally')
  return logger.nominal({ ...Hoek.clone(basePayload), ...Hoek.clone(decorations) })
})

module.exports = decoratePayload
