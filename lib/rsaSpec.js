const Joi = require('joi')

/**
 * @typedef {string} JwksUri
 */
const jwksUri = Joi.string()
  .uri()
  .label('jwksUri')
  .required()
  .description('Well-known uri where public keys can be retrieved')
  .example('https://sandrino.auth0.com/.well-known/jwks.json')

/**
 * @typedef {string} Kid
 */
const kid = Joi.string()
  .label('kid')
  .required()
  .description('Id of the keypair.')
  .example('34ba355c-42ac-4ae2-a5fd-6d61691086f5')

// noinspection SpellCheckingInspection
/**
 * @typedef {string} RsaPublicKey
 */
const publicKey = Joi.string()
  .label('publicKey')
  .description('MUDO') // MUDO
  .example(
    `-----BEGIN RSA PUBLIC KEY-----
MIIBCgKCAQEAjuaPI4gWnkbQH3JKNtTSfkdP36FZuOVC6SQITuNb4nUi0fb2BKn4Ik6LJikz
xhvh3EjbksgXnmv1LEA1y5vZE4z8/OVJLkw+oHGrvhRhzNiCNni+a6ufIcBlyO+ETH3j5bK2
+5St4l9ett7amW7jKfjvC7hxmPBlN/DMSgdSJS3/+U71UMZbxL3SHvYoSewe8ykdlfLph43X
nCe3Le+Ih/CuIMDI5c4RD2bCFDQYtwJZGyRUC0K7HtsnWYjEpHLPoh7DEUM4C2w8nHLxiMno
G2rfYsC64c7M1LDbUP0BUgaZfkOffS+s8Qfnhd33QlVANGdUyQxjqMgimS8RCcnTBQIDAQAB
-----END RSA PUBLIC KEY-----
`
  )

// noinspection SpellCheckingInspection
/**
 * @typedef {string} RsaPrivateKey
 */
const privateKey = Joi.string()
  .label('privateKey')
  .description('MUDO') // MUDO
  .example(
    `-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAjuaPI4gWnkbQH3JKNtTSfkdP36FZuOVC6SQITuNb4nUi0fb2BKn4Ik6L
Jikzxhvh3EjbksgXnmv1LEA1y5vZE4z8/OVJLkw+oHGrvhRhzNiCNni+a6ufIcBlyO+ETH3j
5bK2+5St4l9ett7amW7jKfjvC7hxmPBlN/DMSgdSJS3/+U71UMZbxL3SHvYoSewe8ykdlfLp
h43XnCe3Le+Ih/CuIMDI5c4RD2bCFDQYtwJZGyRUC0K7HtsnWYjEpHLPoh7DEUM4C2w8nHLx
iMnoG2rfYsC64c7M1LDbUP0BUgaZfkOffS+s8Qfnhd33QlVANGdUyQxjqMgimS8RCcnTBQID
AQABAoIBAQCOGbNpL+DeD6jSPIKUN9oBfMRuqzJfbIu27v/cArbSYIz5oc1PIf3/j39LuVkk
vYFB3qmKMrNZ9BzfnhJgoF+i02aXzSGSinsUbTTNVdNTMlF5/WPOCeG6XGpa/+Lddap0Nd1E
G7s2CoRS8RUL0nrOuB5t10IPRa5BjJB5ZQJIuRLJt5T82693od85TZuUg2TKACBYUfFOVqLN
fNDDBI6coMoaIiF8fEJx0mCudLSLWi6MT7DLS7pWbK6CtsPAhAuydYQ9N0/F3kfOaT2o+QEx
P39TRLp5pN6z95DRGsSanJ39l5FvvDH6XvO1dyotKDLDtvOgEuxNnsEWUyko99lZAoGBAMcL
5zIBG0TDvvX/2JVxXOgiYtOWS3paw4/BHB8weZBgjXcnj2+eYS3IUJQyYGPVJk15t+lbPJIa
dm2HNRzESOfgyXVemRvtBQ8mAEbB9pc73Y3k81YbcF9QgAz4VZ4KrNd6NskbWllTfpw5s8Eh
8L4MnPPfUlnEd6VH4d2dP5gPAoGBALfJ/MGez5DT2PkZXobTQKcyDHH0VPWivvqOaGhvo+Qt
pMHy9jUbF8KkWLB3sJOoTQdCrrpi+34h7e8NCE+wzma4WT6iZUzAwTyLUXU6DWO8RC5azUzl
3BrZKgXdW4cdNEfVKDyGkDUVUZ2db4eWiQp2wY72ic6O8WChNTGd06+rAoGAFqIH2+u8Sglo
AVjiK7wEMHEYg66nTnZbnlD6/aDpcb1I2K+q46pCqo4Ie5Fu3Gs0O8MHYoV5UbOom7OwGmFH
WPZ6cdob1s0QsjRD+8e6Xl/0RVovQS9Fi9D/fnoOYjYciTPgXuW5VEbmMqVtBxzw3utYPXK7
TnHEmQqfaC+lN4kCgYBwfQBmwXzpd8BlNlHU05fOvaNAW/tkHKgnUuI0iaAWGJmu27Y16s8w
Isblu1woA+qhdv7atZqYKMwodCGSJPUvicWVwG+f+ppLJDpHNbDcSm6wsILXfhipliFSVZmf
qRBGpwBpizLYSqQZhDKvGkPmU6lEh0DxXCwAqKqWI7Nc4QKBgBcl/X6x63dqAnGv73/v/THC
8wOg85UC5+ZxkJU4HwDcAQlD4rV80q0kwEcySjL+bGf8ntidD3S/YfcNz8nfM+PZVZFwZ4jP
MUi4pznVBjsG3nQBGcRbpOJpAC3WalbFPivRl3Q1CVYoHSBtpJ3Bi8yk9VmF64zqVA0+gJ3F
lATX
-----END RSA PRIVATE KEY-----
`
  )

/**
 * @typedef RsaKeypair
 * @property {string} kid
 * @property {RsaPrivateKey} privateKey
 * @property {RsaPublicKey} privateKey
 */
const keypair = Joi.object({
  id: kid,
  privateKey: privateKey.required(),
  publicKey: publicKey.required()
}).label('keypair')

// noinspection SpellCheckingInspection
const wellKnownKey = Joi.object({
  alg: Joi.string()
    .valid('RS256')
    .required(),
  e: Joi.string()
    .valid('AQAB')
    .required(),
  kid: kid,
  kty: Joi.string()
    .valid('RSA')
    .required(),
  n: Joi.string()
    .required()
    .example(
      'suMc2Y_wLdLMgPltSuho9U_S8IotPLBV33aLN6jfOjx3TtTa-9YowA7stMDsGmhW_Ry08nKW4TeBpCvIYT0Kh7fFrZSKtHisS3pLWlA2sY29z04Xi6SNwG3ac_WyfPSdZIX3zXnTKcw0j8Yg6Lpb5yDx2khcspFptw6ltmD0piYXW7Pp3q5U96iYih3CM8yFMFf34-0lEFQlI74A0svfz2uSl3WRGujWd1tVl1UFAAYBfy4Gwr4Dej2f1AD3pg58J1LFRXWNnL60q567RZwS2wEYoUneRYEC9YT4Y2SMKtHE6aaYww8Av3ureRwqmt0fuA26JVgeBKPYZ-eh-mXH6w'
    ),
  use: Joi.string()
    .valid('sig')
    .required()
})

const wellKnownKeys = Joi.array().items(wellKnownKey)

module.exports = { jwksUri, kid, publicKey, privateKey, keypair, wellKnownKey, wellKnownKeys }
