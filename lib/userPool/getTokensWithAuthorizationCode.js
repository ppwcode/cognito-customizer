const Contract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const common = require('./common')
const ClientOAuth2 = require('client-oauth2')
const validateSchema = require('../_util/validateSchema')
const jwtSpec = require('../jwtSpec')
const jwtError = require('../jwtError')

/**
 * @param {GetUserPoolTokensConfiguration} configuration
 * @param {BasicRequest} request
 * @returns {UserPoolTokens}
 */
const getTokensWithAuthorizationCode = new Contract({
  pre: common.pre.concat([(configuration, logger, request) => request.body.grant_type === 'authorization_code']),
  /* This method makes sure the request follows jwtSpec.clientCredentialsAccessTokenRequest
     if it ends nominally */
  post: common.post.concat([
    (configuration, logger, request) => validateSchema(jwtSpec.authorizationCodeAccessTokenRequest, request)
  ]),
  exception: common.exception
}).implementation(async function getTokensWithAuthorizationCode (configuration, logger, request) {
  logger = logger.start('getTokensWithAuthorizationCode', { configuration, request })
  common.mandatoryBodyParam(request, 'code')
  common.mandatoryBodyParam(request, 'code_verifier', 'this STS only supports grant_type authorization_code with PKCS')
  common.mandatoryBodyParam(request, 'redirect_uri')
  common.mandatoryBodyParam(request, 'client_id')
  if (request.body.scope !== undefined) {
    throw logger.exceptional(
      jwtError.invalid_request(
        'grant_type authorization_code returns scopes requested when acquiring the "code"; ' +
          'to avoid confusion, this STS does not accept a "scope" body parameter'
      )
    )
  }

  const userPool = new ClientOAuth2({
    accessTokenUri: configuration.userPoolTokenEndpoint,
    code: request.body.code,
    redirectUri: request.body.redirect_uri,
    clientId: request.body.client_id,
    body: {
      code_verifier: request.body.code_verifier
    }
    // cognito does not expect audience, no extra body
    // MUDO audience verification?
  })
  try {
    const response = await userPool.code.getToken()
    return logger.nominal(response.data)
  } catch (err) {
    if (err.body && Object.keys(jwtSpec.tokenRequestErrorStatusCodes).includes(err.body.error)) {
      throw logger.exceptional(jwtError(err.body.error, 'user pool denied access'))
    }
    // programming or configuration error, or external error - do not eat
    throw logger.unexpected(err)
  }
})

module.exports = getTokensWithAuthorizationCode
