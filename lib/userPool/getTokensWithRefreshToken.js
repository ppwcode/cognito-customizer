const Contract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const common = require('./common')
const ClientOAuth2 = require('client-oauth2')
const validateSchema = require('../_util/validateSchema')
const jwtSpec = require('../jwtSpec')
const jwtError = require('../jwtError')

/**
 * @param {GetUserPoolTokensConfiguration} configuration
 * @param {BasicRequest} request
 * @returns {UserPoolTokens}
 */
const getTokensWithRefreshToken = new Contract({
  pre: common.pre.concat([(configuration, logger, request) => request.body.grant_type === 'refresh_token']),
  /* This method makes sure the request follows jwtSpec.clientCredentialsAccessTokenRequest
     if it ends nominally */
  post: common.post.concat([
    (configuration, logger, request) => validateSchema(jwtSpec.authorizationCodeAccessTokenRequest, request)
  ]),
  exception: common.exception
}).implementation(async function getTokensWithRefreshToken (configuration, logger, request) {
  logger = logger.start('getTokensWithRefreshToken', { configuration, request })
  common.mandatoryBodyParam(request, 'refresh_token')
  common.mandatoryBodyParam(request, 'client_id')

  const userPool = new ClientOAuth2({
    accessTokenUri: configuration.userPoolTokenEndpoint,
    refreshToken: request.body.refresh_token,
    clientId: request.body.client_id, // MUDO this code adds this as authorization header
    audience: request.body.audience,
    body: {
      client_id: request.body.client_id,
      scope: request.body.scope
    }
    // cognito does not expect audience, no extra body
    // MUDO audience verification?
  })
  try {
    const response = await userPool.refresh.getToken()
    return logger.nominal(response.data)
  } catch (err) {
    if (err.body && Object.keys(jwtSpec.tokenRequestErrorStatusCodes).includes(err.body.error)) {
      throw logger.exceptional(jwtError(err.body.error, 'user pool denied access'))
    }
    // programming or configuration error, or external error - do not eat
    throw logger.unexpected(err)
  }
})

module.exports = getTokensWithRefreshToken
