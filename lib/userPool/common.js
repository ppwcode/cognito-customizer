const validateSchema = require('../_util/validateSchema')
const jwtSpec = require('../jwtSpec')
const Boom = require('boom')
const Joi = require('joi')
const jwtError = require('../jwtError')
const { basicRequest } = require('../commonSpec')
const flowLogger = require('../_util/flowLogger').schema

/**
 * @typedef {object} GetUserPoolTokensConfiguration
 * @property {string} userPoolTokenEndpoint - URI of the token endpoint of the User Pool that backs us
 */
const configurationSchema = Joi.object({
  userPoolTokenEndpoint: Joi.string()
    .uri()
    .required()
    .label('userPoolTokenEndpoint')
    .description('URI of the token endpoint of the User Pool that backs us')
    .example('https://example-user-pool.auth.eu-west-1.amazoncognito.com/oauth2/token')
})
  .unknown(true)
  .label('configuration')
  .required()

/**
 * @typedef {object} UserPoolTokens
 * @property {string} access_token
 * @property {string} [refresh_token]
 * @property {string} [id_token]
 * @property {number} expires
 * @property {string} token_type
 * @property {string} token_type
 */
const tokensSchema = jwtSpec.tokenRequestResponsePayload
  .optionalKeys('refresh_token', 'scope')
  .unknown(true)
  .label('tokens')
  .required()

/**
 * @param {BasicRequest} request
 * @param {string} paramName
 * @param {string} [extraMessage]
 */
function mandatoryBodyParam (request, paramName, extraMessage) {
  if (!request.body[paramName]) {
    throw request.logger.exceptional(
      jwtError.invalid_request(
        `grant_type authorization_code requires "${paramName}" in the www-form-urlencoded body of the request${
          extraMessage ? `${extraMessage}` : ''
        }`
      )
    )
  }
}

module.exports = {
  configurationSchema,
  tokensSchema,
  pre: [
    configuration => validateSchema(configurationSchema, configuration),
    (configuration, logger) => validateSchema(flowLogger, logger),
    (configuration, logger, request) => validateSchema(basicRequest, request)
  ],
  post: [(logger, configuration, request, result) => validateSchema(tokensSchema, result)],
  exception: [
    (configuration, logger, request, exception) => Boom.isBoom(exception),
    (configuration, logger, request, exception) => validateSchema(jwtSpec.tokenRequestErrorOutput, exception.output)
  ],
  mandatoryBodyParam
}
