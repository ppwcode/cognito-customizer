const Contract = require('@toryt/contracts-iv')
const PromiseContract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const validateSchema = require('../_util/validateSchema')
const jwksClient = require('jwks-rsa')
const rsaSpec = require('../rsaSpec')
const { promisify } = require('util')
const { JwksError, SigningKeyNotFoundError, JwksRateLimitError } = require('jwks-rsa')

const cacheMaxAge = 3600 * 1000 // 1h
const resultSchema = rsaSpec.publicKey.label('result').required()

/**
 * Asynchronous function, configured to return a PKCS1 public key for a given `kid` from the configured `jwksUri`.
 * If no keypair is found for the given `kid`, `undefined` is returned.
 *
 * @param {string} kid - The id of the keypair used for signing JWTs.
 * @return {RsaPublicKey} - `undefined` if no keypair is found for `kid`
 */
const signingKeyContract = new PromiseContract({
  pre: [kid => validateSchema(rsaSpec.kid, kid)],
  post: [(kid, result) => validateSchema(resultSchema, result)],
  exception: [
    (kid, exception) =>
      exception instanceof JwksError ||
      exception instanceof JwksRateLimitError ||
      exception instanceof SigningKeyNotFoundError
  ]
})

/**
 * Return an asynchronous function, configured to return a PKCS1 public key for a given `kid` from the
 * configured `jwksUri`. 5 results are cached for 1 hour.
 *
 * @param {string} jwksUri - well-known endpoint for getting the public keys
 */
const signingKeyGetter = new Contract({
  pre: [jwksUri => validateSchema(rsaSpec.jwksUri, jwksUri)],
  post: [(jwksUri, result) => signingKeyContract.isImplementedBy(result)]
}).implementation(function (jwksUri) {
  const client = jwksClient({
    cache: true,
    cacheMaxEntries: 5,
    cacheMaxAge: cacheMaxAge,
    rateLimit: true,
    jwksUri: jwksUri
  })
  const getSigningKey = promisify(client.getSigningKey)

  return signingKeyContract.implementation(async function getUserPoolSigningKey (kid) {
    const response = await getSigningKey(kid)
    return response.publicKey || response.rsaPublicKey
  })
})

signingKeyGetter.getSigningKey = signingKeyContract

module.exports = signingKeyGetter
