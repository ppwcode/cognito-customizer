const Contract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const common = require('./common')
const ClientOAuth2 = require('client-oauth2')
const validateSchema = require('../_util/validateSchema')
const jwtSpec = require('../jwtSpec')
const auth = require('basic-auth')
const jwtError = require('../jwtError')

/**
 * @typedef {object} Credentials
 * @property {string} name
 * @property {string} pass
 */

/**
 * @param {GetUserPoolTokensConfiguration} configuration
 * @param {BasicRequest} request
 * @returns {UserPoolTokens}
 */
const getTokensWithClientCredentials = new Contract({
  pre: common.pre.concat([(configuration, logger, request) => request.body.grant_type === 'client_credentials']),
  /* This method makes sure the request follows jwtSpec.clientCredentialsAccessTokenRequest
     if it ends nominally */
  post: common.post.concat([
    (configuration, logger, request) => validateSchema(jwtSpec.clientCredentialsAccessTokenRequest, request)
  ]),
  exception: common.exception
}).implementation(async function getTokensWithClientCredentials (configuration, logger, request) {
  logger = logger.start('getTokensWithClientCredentials', { configuration, request })
  const credentials = /** @type Credentials */ auth(request)
  if (!credentials || !credentials.name || !credentials.pass) {
    throw logger.exceptional(jwtError.invalid_client('no client credentials found in Authorization header'))
  }
  const scopes = request.body.scope ? request.body.scope.split(' ') : []
  const userPool = new ClientOAuth2({
    accessTokenUri: configuration.userPoolTokenEndpoint,
    clientId: credentials.name,
    clientSecret: credentials.pass,
    scopes: scopes
    // cognito does not expect audience, no extra body
  })
  try {
    const response = await userPool.credentials.getToken()
    return logger.nominal(response.data)
  } catch (err) {
    if (err.body && Object.keys(jwtSpec.tokenRequestErrorStatusCodes).includes(err.body.error)) {
      throw logger.exceptional(jwtError(err.body.error, 'user pool denied access'))
    }
    // programming or configuration error, or external error - do not eat
    throw logger.unexpected(err)
  }
})

module.exports = getTokensWithClientCredentials
