const Contract = require('@toryt/contracts-iv/lib/IV/PromiseContract')
const validateSchema = require('../_util/validateSchema')
const commonConditions = require('./common')
const jwtSpec = require('../jwtSpec')
const getTokensWithAuthorizationCode = require('./getTokensWithAuthorizationCode')
const getTokensWithRefreshToken = require('./getTokensWithRefreshToken')
const getTokensWithClientCredentials = require('./getTokensWithClientCredentials')
const jwtError = require('../jwtError')

const implementations = {
  authorization_code: getTokensWithAuthorizationCode,
  refresh_token: getTokensWithRefreshToken,
  client_credentials: getTokensWithClientCredentials
}

/**
 * Forward the token request to the User Pool, and return the refresh and id tokens we receive.
 * Accepts any request, because the response to illegal requests is defined behavior.
 *
 * @param {GetUserPoolTokensConfiguration} configuration
 * @param {AccessTokenRequest} request
 * @returns {UserPoolTokens}
 */
const getTokens = new Contract({
  pre: commonConditions.pre,
  /* This method makes sure the request follows jwtSpec.accessTokenRequest if it ends nominally */
  post: commonConditions.post.concat([
    (configuration, logger, request) => validateSchema(jwtSpec.accessTokenRequest, request)
  ]),
  exception: commonConditions.exception
}).implementation(async function getTokens (config, logger, request) {
  logger = logger.start('getTokens', { config, request })
  if (!(request.body.grant_type in implementations)) {
    throw logger.exceptional(
      jwtError.unsupported_grant_type(
        `grant_type "${request.body.grant_type}" is not supported. Supported types are: "${Object.keys(
          implementations
        ).join('", "')}".`
      )
    )
  }
  return logger.nominal(implementations[request.body.grant_type](config, logger, request))
})

module.exports = getTokens
